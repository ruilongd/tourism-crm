<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
</head>
<body>



<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>
<block>
    <link rel="stylesheet" href="/Public/statics/layui/css/layui.css"  media="all">
    <style>
        .col-xs-3{margin-top:5px;}
        .col-xs-4{
            text-align: center;
            height: 34px;
            width: 35%;
            line-height: 34px;
            width: 35%!important;
        }
        .col-xs-8{
            width: 63%!important; 
        }
         .margin-top0{
            margin-top: 0px!important;
        }
        th{text-align: center;}
    </style>
</block>

    <div class="page-header"><h1>首页 &gt;商家管理 > 注册供应商列表</h1></div>
    <form action="" method="get" >
        <div class="col-xs-12 ">
            <div class="col-xs-3">
                <label for="" class="col-xs-4">供应商编号:</label>
                <div class="col-xs-8 text-left"><input type="text" class="form-control" name="supplier_sn" placeholder="请输供应商编号" value="<?php echo ($post["supplier_sn"]); ?>" /></div>
            </div>
            <div class="col-xs-3">
                <label for="" class="col-xs-4">供应商名称:</label>
                <div class="col-xs-8 text-left"><input type="text" class="form-control" name="supplier_name" placeholder="请输入供应商名称"  value="<?php echo ($post["supplier_name"]); ?>"/></div>
            </div>
            <div class="col-xs-3" style="display: none;">
                <label for="" class="col-xs-4">状态:</label>
                <div class="col-xs-8 text-left">
                    <select class="form-control" name="status" id="">
                        <option value="1" <?php if($post['status'] == 1): ?>selected<?php endif; ?> >合作</option>
                        <option value="-1" <?php if($post['status'] == -1): ?>selected<?php endif; ?>>停止合作</option>
                    </select>
                </div>
            </div>

            <!-- <div class="col-xs-3">
                 <label for="" class="col-xs-4">合作开始时间:</label>
                 <div class="col-xs-8 text-left"><input id="start_time" name="start_time" value="<?php echo ($post["start_time"]); ?>" type="text" readonly class="form-control day" placeholder="合作开始时间" /></div>

             </div>

             <div class="col-xs-3">
                 <label for="" class="col-xs-4">合作结束时间:</label>
                 <div class="col-xs-8 text-left"><input type="text" name="end_time" id="end_time"  value="<?php echo ($post["end_time"]); ?>" readonly class="form-control day" placeholder="合作结束时间" /></div>
             </div>-->

            <div class="col-xs-3 margin-top0">
                <input class="btn btn-primary" type="submit" value="查询"/>
            </div>
        </div>

        <div class="col-xs-12"  style="margin-top:10px;">
            <!--  <button class="btn btn-primary btn-xs pull-right" style="margin-right: 10px">添加新的供应商</button>-->
            <?php if(checkAuth('Operator/Shops/lineShopsExportExcel')): ?><a class="btn btn-primary btn-xs pull-right" style="margin-right: 10px" href="<?php echo U('lineShopsExportExcel');?>">导出</a><?php endif; ?>
        </div>

    </form>

    <table class="table table-striped table-bordered table-hover table-condensed text-center">
        <thead>
        <tr>
            <th class="filtrate_1" >供应商名称</th>
            <th class="filtrate_2" >供应商账号</th>
            <th class="filtrate_3" >供应商编号 <!--<input type="checkbox" class="checkAll">--></th>
            <th class="filtrate_4" >移动电话</th>
            <th class="filtrate_5" >联系人</th>
            <th class="filtrate_6" >地址</th>
            <th class="filtrate_7" >有效时间</th>
            <th class="filtrate_8" >截止时间</th>
            <th class="filtrate_9" >注册时间</th>
            <th class="filtrate_10" >服务等级</th>
            <th class="filtrate_11" >帐户状态</th>
            <th class="filtrate_13" >审核状态</th>
            <th class="filtrate_12"  style="width:180px">操作</th>
        </tr>
        </thead>
        <tbody>
        <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr >
                <td class="filtrate_1" ><?php echo ($vo["supplier_name"]); ?> <!--<input type="checkbox" class="chk">--></td>
                <td class="filtrate_2"><?php echo ($vo["supplier_account"]); ?></td>
                <td class="filtrate_3"><?php echo ($vo["supplier_sn"]); ?></td>
                <td class="filtrate_4"><?php echo ($vo["supplier_phone"]); ?></td>
                <td class="filtrate_5"><?php echo ($vo["linkman"]); ?></td>
                <td class="filtrate_6"><?php echo ($vo["complete_address"]); ?></td>
                <td class="filtrate_7"><?php echo (date('Y-m-d',$vo["start_time"])); ?></td>
                <td class="filtrate_8"><?php echo (date('Y-m-d',$vo["end_time"])); ?></td>
                <td class="filtrate_9"><?php echo (date('Y-m-d',$vo["create_time"])); ?></td>
                <td class="filtrate_10"><?php echo ($vo["service_level"]); ?></td>
                <td class="filtrate_11">
                    <?php switch($vo["supplier_status"]): case "1": ?>开通<?php break;?>
                        <?php case "-1": ?>锁定<?php break; endswitch;?>

                </td>
                <td class="filtrate_13">
                    <?php switch($vo["is_review"]): case "-1": ?>未审核<?php break;?>
                        <?php case "1": ?>已通过<?php break;?>
                        <?php case "-2": ?>已拒绝<?php break; endswitch;?>
                </td>
                <td class="filtrate_12">
                    <?php if(checkAuth('Operator/Shops/addLineShops')): ?><a href="<?php echo U('addLineShops',array('id'=>$vo['supplier_id']));?>" class="btn btn-xs btn-primary">编辑</a><?php endif; ?>
                    <?php if(checkAuth('Operator/Shops/changeSupplierStatusById')): if($vo['supplier_status'] == 1): ?><a href="<?php echo U('changeSupplierStatusById',array('id'=>$vo['supplier_id'],'supplier_status'=>-1));?>" class="btn btn-xs btn-warning">锁定</a>
                            <?php else: ?>
                            <a href="<?php echo U('changeSupplierStatusById',array('id'=>$vo['supplier_id'],'supplier_status'=>1));?>" class="btn btn-xs btn-primary">开通</a><?php endif; endif; ?>
                </td>
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>

        <tr>
            <td colspan="13">
                <!--分页样式-->
                <ul class="pagination">
                    <?php echo ($show); ?>
                </ul>
            </td>
        </tr>
        </tbody>
    </table>


    <script src=""></script>
    <script src="/Public/statics/layui/layui.all.js"></script>
    <script src="/Public/statics/layui/layui.js"></script>
    <script>
        /**
         * 打开订单详情页面
         * @param  {[int]}    orderId     [订单id]
         */

        //筛选显示
        $('body').on('change','.filtrate',function () {
            var id=$(this).val();
            if($(this).is(':checked')){
                $('.filtrate_'+id).show();
            }else{
                $('.filtrate_'+id).hide();
            }
        });


        layui.use('laydate', function() {
            var laydate = layui.laydate;
            //常规用法
            laydate.render({
                elem: '#start_time'
            });
            laydate.render({
                elem: '#end_time'
            });
        })

        /**
         *订单详情
         * @param orderId
         */

        function openDetail(orderId){
            layer.open({
                type: 2,
                offset:20,
                area: ['90%','90%'],
                title: '订单详情',
                content: '<?php echo U("Orders/detail");?>',
            })
        }
    </script>