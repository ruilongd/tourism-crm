<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>
        
    </title>
    <meta name="keywords" content=""/>
    <meta name="description" content=""/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link href="/Public/statics/aceadmin/css/bootstrap.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="/Public/statics/font-awesome-4.4.0/css/font-awesome.min.css"/>
    <!--[if IE 7]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/font-awesome-ie7.min.css"/><![endif]-->
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace.min.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/Public/statics/aceadmin/css/ace-ie.min.css"/><![endif]--><!--[if lt IE 9]>
    <script src="/Public/statics/aceadmin/js/html5shiv.js"></script>
    <script src="/Public/statics/aceadmin/js/respond.min.js"></script><![endif]-->
    <!-- <link rel="stylesheet" href="/Public/css/base.css"/> -->
    <style>
        ::-webkit-scrollbar {
            width: 10px;
            height: 5px;
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 0;
            background-color: rgba(0,0,0,.3);
        }

        ::-webkit-scrollbar-corner, ::-webkit-scrollbar-track {
            background-color: #e2e2e2;
        }
        ul,li{ list-style: none; }
        ol{margin:0;}
        .jedatehms li{display: none;}
        #jedatebox ul{
            padding-right: 0;
            margin-right: 0;
        }
    </style>
    
    <link rel="stylesheet" href="/Public/statics/Dealer/css/tabstyle.css"/>
    <link rel="stylesheet" href="/Public/statics/layui/css/layui.css"  media="all">
    <style>
        .type-item {
            background: #ccc;
            min-width: 100px;
            text-align: center;
            padding: 5px 10px;
        }
        .row{padding:5px;}
    </style>

</head>
<body>

 <div class="page-header" style="background-color: #fff!important"><h1><i class="fa fa-home"></i> 首页 &gt;产品管理&gt;线路发布</h1></div>

    <ul id="nav" class="nav nav-tabs">
        <li role="presentation" class="active"><a href="javascript:void(0)">第一步基本信息</a></li>
        <li role="presentation" class=""><a href="javascript:void(0)">第二步价格计划</a></li>
        <li role="presentation" class=""><a href="javascript:void(0)">第三步行程安排</a></li>
        <li role="presentation" class=""><a href="javascript:void(0)">第四步签证添加</a></li>
        <li role="presentation" class=""><a href="javascript:void(0)">第五步附加产品</a></li>
        <li role="presentation" ><a href="javascript:void(0)">销售渠道</a></li>
    </ul>

    <div class="tab-content">
        <div class="panelsd">
            <!-- 基本信息 -->
            <div class="paneld">
            <form class="myForm" method="post"  action='<?php echo U("step1_action");?>'>

                <div class="row row-div">
                    <div class="col-md-2"><p class="text-right"><font color='red'>*</font>线路名称：</p></div>
                    <div class="col-md-5">
                        <input class="form-input" placeholder="城市+某景点+交通方式+时间,最多50个字符" required="required"  type="text" name="line_name" id="line_name" maxlength="50" value="<?php echo ($lineInfo['line_name']); ?>">
                        <div class="explain">
                            <span>城市+某景点+交通方式+时间,最多50个字符</span>
                        </div>
                    </div>
                </div>
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>团号：</p></div>
                    <div class="col-md-5">
                        <input class="form-input" placeholder="请输入团号"  required="required" type="text" name="group_num" id="group_num" maxlength="150" value="<?php echo ($lineInfo['group_num']); ?>"/>
                    </div>
                </div>
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>行程天数：</p></div>
                    <div class="col-md-5">
                        <input class="form-input" placeholder="请输入行程天数" required="required" type="tel" name="travel_days" id="group_num" maxlength="150"  value="<?php echo ($lineInfo['travel_days']); ?>" onkeyup="this.value=this.value.replace(/\D/gi,'')" onafterpaste="clearNoNum(this)" />
                    </div>
                </div>
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>出发城市：</p></div>
                    <div class="col-md-5">
                        <!-- <input type="text" name="group_num" id="group_num" maxlength="150"> -->
                        <select id='origin_id' name="origin_id" required>
                            <option value=''>请选择出发城市</option>
                            <?php if(is_array($areas_info)): $i = 0; $__LIST__ = $areas_info;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option value="<?php echo ($vo['areaId']); ?>" <?php if($vo['areaId'] == $lineInfo['origin_id']): ?>selected="true"<?php endif; ?> ><?php echo ($vo['areaName']); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
                    </div>

                </div>

                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>目的地城市：</p></div>
                    <input type="hidden" name="destination_id" id="destination_id" value="<?php echo ($lineInfo["destination_id"]); ?>">
                    <div class="col-md-5">
                        <input type="text" class="form-input" placeholder="点击选择目的地" onclick="choseDestinationCity()"  value="<?php if(is_array($destinationName)): $i = 0; $__LIST__ = $destinationName;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; echo ($vo["areaName"]); ?>,<?php endforeach; endif; else: echo "" ;endif; ?>  " readonly id="destination" required>
                    </div>

                </div>

                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>线路主题：</p></div>
                    <div class="col-md-10">
                        <?php if(is_array($themes)): $i = 0; $__LIST__ = $themes;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><input class="form-radio" type='checkbox' width="30" class="theme" id='theme_name<?php echo ($vo["theme_id"]); ?>' name='theme[]' <?php if(in_array($vo['theme_id'],$checkedTheme)): ?>checked<?php endif; ?> value="<?php echo ($vo['theme_id']); ?>"/><span for="{theme_name{$vo.theme_id}}"><?php echo ($vo['theme_name']); ?></span><?php endforeach; endif; else: echo "" ;endif; ?>
                    </div>
                </div>
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>预计收客人数：</p></div>
                    <div class="col-md-5">
                        <input class="form-input" required="required" placeholder="请填写预计收客人数"  type="tel" name="guests_num" id="guests_num" maxlength="150" onkeyup="this.value=this.value.replace(/\D/gi,'')" value="<?php echo ($lineInfo['guests_num']); ?>" />
                    </div>
                </div>
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>截止收团:</p></div>
                    <div class="col-md-5">
                       出团日期 <input type="tel"  required="required" name="till_day" id="till_day" maxlength="150" onkeyup="this.value=this.value.replace(/\D/gi,'')"  value="<?php echo ($lineInfo['till_day']); ?>" />&nbsp;天前截止
                    </div>
                </div>
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>预订时间:</p></div>
                    <div class="col-md-5">
                       出团日期 <input type="tel" required="required" name="book_day" id="book_day" maxlength="150" onkeyup="this.value=this.value.replace(/\D/gi,'')" value="<?php echo ($lineInfo['book_day']); ?>" />&nbsp;天前开始预定
                    </div>
                </div>
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>出团日期:</p></div>
                    <div class="col-md-2" >
                        是否天天出团:
                        <select name="is_everyday" id="is_everyday">
                            <option value="1" <?php if($lineInfo['is_everyday'] == 1): ?>selected<?php endif; ?> >是</option>
                            <option value="0" <?php if($lineInfo['is_everyday'] == 0): ?>selected<?php endif; ?>>否</option>
                        </select>
                    </div>

                    <div class="col-md-8" >

                        <div id="group-section" style="display: <?php if($lineInfo['is_everyday'] == 1): ?>block<?php else: ?>none<?php endif; ?>" >
                            <input type="text" name="start_time"   class="form-control form-date left-float" id="start_time" placeholder="开始日期" value="<?php echo (date("Y-m-d",(isset($lineInfo['start_time']) && ($lineInfo['start_time'] !== ""))?($lineInfo['start_time']):time()+86400)); ?>">
                            <span class="left-float date-center">至</span>
                            <input type="text" name="end_time" class="form-control form-date left-float" id="end_time" placeholder="结束日期" value="<?php echo (date("Y-m-d",(isset($lineInfo['end_time']) && ($lineInfo['end_time'] !== ""))?($lineInfo['end_time']):time()+86400)); ?>">
                        </div>

                        <div id="group-day" style="display: <?php if($lineInfo['is_everyday'] == 0): ?>block<?php else: ?>none<?php endif; ?>">
                            <span for="forum_time">出团日期:</span>
                            <input type="text" name="forum_time" onclick="forumTime()" id="forum_time" value="<?php echo ($lineInfo["forum_time"]); ?>"  class="form-input" placeholder="点击选择日期" readonly />
                        </div>

                    </div>
                </div>
        <hr>
                <!--去的交通方式-->
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>往返交通(出发):</p></div>
                    <div class="col-md-10">
                        <p>
                             *出发地:
                            国家
                            <select  id="go_country_id"  onchange="getProvince(this,'go')" >
                                <option value="">==选择国家==</option>
                                <?php if(is_array($goTrafficCity["start_country"])): $i = 0; $__LIST__ = $goTrafficCity["start_country"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($goTraffic['start_country'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            省
                            <select  id="go_province_id"  onchange="getCity(this,'go')" >
                                <option value="">==选择省==</option>
                                <?php if(is_array($goTrafficCity["start_province"])): $i = 0; $__LIST__ = $goTrafficCity["start_province"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($goTraffic['start_province'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            市
                            <select id="go_city_id"  onchange="getArea(this,'go')" >
                                <option value="">==选择市==</option>
                                <?php if(is_array($goTrafficCity["start_city"])): $i = 0; $__LIST__ = $goTrafficCity["start_city"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($goTraffic['start_city'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            区
                            <select  id="go_area_id" >
                                <option value="">==选择区==</option>
                                <?php if(is_array($goTrafficCity["start_area"])): $i = 0; $__LIST__ = $goTrafficCity["start_area"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($goTraffic['start_area'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </p>

                        <p>
                            *目的地:
                            国家
                            <select  id="go_end_country_id"  onchange="getProvince(this,'go_end')" >
                                <option value="">==选择国家==</option>
                                <?php if(is_array($goTrafficCity["goal_country"])): $i = 0; $__LIST__ = $goTrafficCity["goal_country"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($goTraffic['goal_country'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            省
                            <select id="go_end_province_id"  onchange="getCity(this,'go_end')" >
                                <option value="">==选择省==</option>
                                <?php if(is_array($goTrafficCity["goal_province"])): $i = 0; $__LIST__ = $goTrafficCity["goal_province"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($goTraffic['goal_province'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            市
                            <select id="go_end_city_id"  onchange="getArea(this,'go_end')" >
                                <option value="">==选择市==</option>
                                <?php if(is_array($goTrafficCity["goal_city"])): $i = 0; $__LIST__ = $goTrafficCity["goal_city"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($goTraffic['goal_city'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            区
                            <select  id="go_end_area_id" >
                                <option value="">==选择区==</option>
                                <?php if(is_array($goTrafficCity["goal_area"])): $i = 0; $__LIST__ = $goTrafficCity["goal_area"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($goTraffic['goal_area'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            <span class="btn btn-xs btn-primary" onclick="choseGoTraffic();">选择交通方式</span>
                        </p>
                        <if>
                            <p class="text-primary"> 已选择出发交通方式:</p>
                            <span class="label label-success" style=" <?php if(!$goTraffic): ?>display: none;<?php endif; ?> " id="go_price_id_info" >
                                <?php echo ($goTraffic["supplier_name"]); ?>(<?php echo ($goTraffic["money_type"]); ?>)-<?php echo ($goTraffic["traffic_name"]); ?>|<?php echo ($goTraffic["seat_name"]); ?>-<?php echo ($goTraffic["routes_name"]); ?>-座位数<?php echo ($goTraffic["seat_num"]); ?>-成人价<?php echo ($goTraffic["adult_actual_cost"]); ?>-儿童价<?php echo ($goTraffic["child_actual_cost"]); ?>
                            </span>

                        <div id="go_upgrade_traffic_parent" <?php if(!$upgradeTrafficList): ?>style="display: none;"<?php endif; ?> >
                            <h4><b class="text-primary">可升级选项:</b></h4>
                            <div  id="go_upgrade_traffic" >
                                <?php if(is_array($upgradeTrafficList)): $i = 0; $__LIST__ = $upgradeTrafficList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo['type'] == 1): ?><p>
                                            <span for="" class="text-warning"><?php echo ($vo["seat_name"]); ?> 儿童成本<?php echo ($vo["origin_child_price"]); ?> 成人成本<?php echo ($vo["origin_adult_price"]); ?></span>
                                            销售成人价: <input type="text" value="<?php echo ($vo["now_adult_price"]); ?>" name="go_upgrade_traffic[<?php echo ($vo["traffic_price_id"]); ?>][now_adult_price]"  onkeyup="clearNoNum(this)"> &nbsp;&nbsp;
                                            销售儿童价: <input type="text" value="<?php echo ($vo["now_child_price"]); ?>" name="go_upgrade_traffic[<?php echo ($vo["traffic_price_id"]); ?>][now_child_price]"  onkeyup="clearNoNum(this)">
                                        </p><?php endif; endforeach; endif; else: echo "" ;endif; ?>

                            </div>
                        </div>

                        <input type="hidden" name="go_price_id" id="go_price_id" value="<?php echo ($lineInfo["go_price_id"]); ?>">
                        <!--<input type="text" class="form-input" id="go_price_id_info" placeholder="已选交通方式"  value="<?php echo ($goTraffic["supplier_name"]); ?>-<?php echo ($goTraffic["traffic_name"]); ?>-<?php echo ($goTraffic["routes_name"]); ?>-座位<?php echo ($goTraffic["seat_num"]); ?>-成人<?php echo ($goTraffic["adult_actual_cost"]); ?>-儿童<?php echo ($goTraffic["child_actual_cost"]); ?>" readonly/>-->
                        </p>

                    </div>
                </div>
                <!--去的交通方式结束-->


                <!--回来的交通方式-->
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>往返交通(回来):</p></div>
                    <div class="col-md-10">
                        <p>
                            *出发地:
                            国家
                            <select  id="back_country_id"  onchange="getProvince(this,'back')" >
                                <option value="">==选择国家==</option>
                                <?php if(is_array($backTrafficCity["start_country"])): $i = 0; $__LIST__ = $backTrafficCity["start_country"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($backTraffic['start_country'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            省
                            <select  id="back_province_id"  onchange="getCity(this,'back')" >
                                <option value="">==选择省==</option>
                                <?php if(is_array($backTrafficCity["start_province"])): $i = 0; $__LIST__ = $backTrafficCity["start_province"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($backTraffic['start_province'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            市
                            <select id="back_city_id"  onchange="getArea(this,'back')" >
                                <option value="">==选择市==</option>
                                <?php if(is_array($backTrafficCity["start_city"])): $i = 0; $__LIST__ = $backTrafficCity["start_city"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($backTraffic['start_city'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            区
                            <select  id="back_area_id" >
                                <option value="">==选择区==</option>
                                <?php if(is_array($backTrafficCity["start_area"])): $i = 0; $__LIST__ = $backTrafficCity["start_area"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($backTraffic['start_area'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                        </p>

                        <p>
                            *目的地:
                            国家
                            <select  id="back_end_country_id"  onchange="getProvince(this,'back_end')" >
                                <option value="">==选择国家==</option>
                                <?php if(is_array($backTrafficCity["goal_country"])): $i = 0; $__LIST__ = $backTrafficCity["goal_country"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($backTraffic['goal_country'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            省
                            <select id="back_end_province_id"  onchange="getCity(this,'back_end')" >
                                <option value="">==选择省==</option>
                                <?php if(is_array($backTrafficCity["goal_province"])): $i = 0; $__LIST__ = $backTrafficCity["goal_province"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($backTraffic['goal_province'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            市
                            <select id="back_end_city_id"  onchange="getArea(this,'back_end')" >
                                <option value="">==选择市==</option>
                                <?php if(is_array($backTrafficCity["goal_city"])): $i = 0; $__LIST__ = $backTrafficCity["goal_city"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($backTraffic['goal_city'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            区
                            <select  id="back_end_area_id" >
                                <option value="">==选择区==</option>
                                <?php if(is_array($backTrafficCity["goal_area"])): $i = 0; $__LIST__ = $backTrafficCity["goal_area"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($backTraffic['goal_area'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            <span class="btn btn-xs btn-primary" onclick="choseBackTraffic();">选择交通方式</span>
                        </p>
                        <p>
                            <p class="text-primary">已选择回来的交通方式:</p>
                            <span class="label label-success" style="<?php if(!$backTraffic): ?>display: none;<?php endif; ?>" id="back_price_id_info" >
                                <?php echo ($backTraffic["supplier_name"]); ?>(<?php echo ($backTraffic["money_type"]); ?>)-<?php echo ($backTraffic["traffic_name"]); ?>|<?php echo ($backTraffic["seat_name"]); ?>-<?php echo ($backTraffic["routes_name"]); ?>-座位数<?php echo ($backTraffic["seat_num"]); ?>-成人价<?php echo ($backTraffic["adult_actual_cost"]); ?>-儿童价<?php echo ($backTraffic["child_actual_cost"]); ?>
                            </span>

                            <div id="back_upgrade_traffic_parent" <?php if(!$upgradeTrafficList): ?>style="display: none;"<?php endif; ?>>
                                    <h4><b class="text-primary">可升级选项:</b></h4>
                                <div  id="back_upgrade_traffic" >
                                    <?php if(is_array($upgradeTrafficList)): $i = 0; $__LIST__ = $upgradeTrafficList;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo['type'] == 2): ?><p>
                                                <span for="" class="text-warning"><?php echo ($vo["seat_name"]); ?> 儿童成本<?php echo ($vo["origin_child_price"]); ?> 成人成本<?php echo ($vo["origin_adult_price"]); ?></span>
                                                销售成人价: <input type="text" value="<?php echo ($vo["now_adult_price"]); ?>" name="back_upgrade_traffic[<?php echo ($vo["traffic_price_id"]); ?>][now_adult_price]"  onkeyup="clearNoNum(this)"> &nbsp;&nbsp;
                                                销售儿童价: <input type="text" value="<?php echo ($vo["now_child_price"]); ?>" name="back_upgrade_traffic[<?php echo ($vo["traffic_price_id"]); ?>][now_child_price]"  onkeyup="clearNoNum(this)">
                                            </p><?php endif; endforeach; endif; else: echo "" ;endif; ?>
                                </div>
                             </div>


                             <input type="hidden" name="back_price_id" id="back_price_id" value="<?php echo ($lineInfo["back_price_id"]); ?>">


                            <!--<input type="text" class="form-input" id="back_price_id_info" placeholder="已选交通方式" value="<?php echo ($backTraffic["supplier_name"]); ?>-<?php echo ($backTraffic["traffic_name"]); ?>-<?php echo ($backTraffic["routes_name"]); ?>-座位<?php echo ($backTraffic["seat_num"]); ?>-成人<?php echo ($backTraffic["adult_actual_cost"]); ?>-儿童<?php echo ($backTraffic["child_actual_cost"]); ?>" readonly/>-->
                        </p>

                    </div>
                </div>
                <!--回来的交通方式结束-->

        <hr>

                <!--选择酒店-->
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>酒店选择:</p></div>
                    <div class="col-md-10">
                        <p>
                            *所在地:
                            国家
                            <select  id="hotel_country_id"  onchange="getProvince(this,'hotel')" >
                                <option value="">==选择国家==</option>
                                <?php if(is_array($hotelCity["country"])): $i = 0; $__LIST__ = $hotelCity["country"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($checkedHotel[0]['country'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            省
                            <select  id="hotel_province_id"  onchange="getCity(this,'hotel')" >
                                <option value="">==选择省==</option>
                                <?php if(is_array($hotelCity["province"])): $i = 0; $__LIST__ = $hotelCity["province"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($checkedHotel[0]['province'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            市
                            <select id="hotel_city_id"  onchange="getArea(this,'hotel')" >
                                <option value="">==选择市==</option>
                                <?php if(is_array($hotelCity["city"])): $i = 0; $__LIST__ = $hotelCity["city"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($checkedHotel[0]['city'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            区
                            <select  id="hotel_area_id" >
                                <option value="">==选择区==</option>
                                <?php if(is_array($hotelCity["area"])): $i = 0; $__LIST__ = $hotelCity["area"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($checkedHotel[0]['area'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            <span class="btn btn-xs btn-primary" onclick="choseHotel()">添加酒店</span>
                        </p>

                        <input type="hidden" value="<?php echo ($lineInfo["room_price_id"]); ?>" id="room_price_id" name="room_price_id" >
                        <p id="room_price_id_name">
                            已选择的酒店:
                            <?php if(is_array($checkedHotel)): $i = 0; $__LIST__ = $checkedHotel;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><span  class="label label-success room_price_id" data-id="<?php echo ($vo["room_priceId"]); ?>"><?php echo ($vo["supplier_name"]); ?>(<?php echo ($vo["money_type"]); ?>)-<?php echo ($vo["room_name"]); ?>-<?php echo ($vo["star_level"]); ?>星级-房间数<?php echo ($vo["room_count"]); ?>-成人价<?php echo ($vo["adult_actual_cost"]); ?>-儿童价<?php echo ($vo["child_actual_cost"]); ?>[删除]</span><?php endforeach; endif; else: echo "" ;endif; ?>

                           <!-- <span  class="label label-success room_price_id" data-id="1">天河酒店[删除]</span>
                            <span  class="label label-success room_price_id" data-id="1">天河酒店[删除]</span>-->
                        </p>
                    </div>
                </div>

                <!--给运营商的价格-->
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>酒店打包价格：</p></div>
                    <div class="col-md-5">
                        <input class="form-input" required="required" placeholder="酒店打包给销售的价格"  type="tel" name="hotel_price" id="hotel_price" maxlength="50" onkeyup='clearNoNum(this)' value="<?php echo ($lineInfo['hotel_price']); ?>" /><?php echo ($supplierInfo['currency']); ?>
                    </div>
                </div>

                <!--酒店可选升级服务-->
                <div class="row row-div">
                    <div class="col-md-2" >
                        <p class="text-right"><font color='red'>*</font>酒店升级收费: </p>
                    </div>
                    <div class="col-md-10">
                        <span class="btn btn-xs btn-primary" onclick="upgradeHotel()">添加可升级服务酒店</span>
                        <div id="upgradeHotelList">
                            <?php if(is_array($upgradeHotel)): $i = 0; $__LIST__ = $upgradeHotel;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><div class="row">
                                    <span >
                                        <span class="text-warning"><?php echo ($vo["supplier_name"]); ?>(<?php echo ($vo["currency"]); ?>)-<?php echo ($vo["room_name"]); ?>-<?php echo ($vo["star_level"]); ?>星级-成人价<?php echo ($vo["adult_actual_cost"]); ?>-儿童价<?php echo ($vo["child_actual_cost"]); ?>-房间数<?php echo ($vo["room_count"]); ?>:</span>
                                        <input type="text" placeholder="请输入金额!" name="upgrade[<?php echo ($vo["room_price_id"]); ?>][to_reseller_price]" value="<?php echo ($vo["to_reseller_price"]); ?>"   onkeyup="clearNoNum(this)" onafterpaste="clearNoNum(this)" required>
                                    </span>
                                    <span class="btn btn-xs btn-primary delUpgradeHotel">删除</span>
                                </div><?php endforeach; endif; else: echo "" ;endif; ?>
                        </div>
                    </div>
                </div>





                 <hr>
                <!--选择景区-->
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>景区选择:</p></div>
                    <div class="col-md-10">
                        <p>
                            *所在地:
                            国家
                            <select  id="scenic_country_id"  onchange="getProvince(this,'scenic')" >
                                <option value="">==选择国家==</option>
                                <?php if(is_array($scenicCity["country"])): $i = 0; $__LIST__ = $scenicCity["country"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($checkedScenic[0]['country'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            省
                            <select  id="scenic_province_id"  onchange="getCity(this,'scenic')" >
                                <option value="">==选择省==</option>
                                <?php if(is_array($scenicCity["province"])): $i = 0; $__LIST__ = $scenicCity["province"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($checkedScenic[0]['province'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            市
                            <select id="scenic_city_id"  onchange="getArea(this,'scenic')" >
                                <option value="">==选择市==</option>
                                <?php if(is_array($scenicCity["city"])): $i = 0; $__LIST__ = $scenicCity["city"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($checkedScenic[0]['city'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            区
                            <select  id="scenic_area_id" >
                                <option value="">==选择区==</option>
                                <?php if(is_array($scenicCity["area"])): $i = 0; $__LIST__ = $scenicCity["area"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><option <?php if($checkedScenic[0]['area'] == $vo['areaId']): ?>selected<?php endif; ?> value="<?php echo ($vo["areaId"]); ?>"><?php echo ($vo["areaName"]); ?></option><?php endforeach; endif; else: echo "" ;endif; ?>
                            </select>
                            <span class="btn btn-xs btn-primary" onclick="choseScenic()">添加景区</span>
                        </p>

                        <input type="hidden" value="<?php echo ($lineInfo["scenic_price_id"]); ?>" id="scenic_price_id" name="scenic_price_id">
                        <p id="scenic_price_id_name">
                            已选择的景区:
                            <?php if(is_array($checkedScenic)): $i = 0; $__LIST__ = $checkedScenic;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><span  class="label label-success scenic_price_id" data-id="<?php echo ($vo["scenic_priceId"]); ?>"><?php echo ($vo["supplier_name"]); ?>(<?php echo ($vo["money_type"]); ?>)-<?php echo ($vo["scenic_name"]); ?>-<?php echo ($vo["scenic_level"]); ?>A级-成人价<?php echo ($vo["adult_actual_cost"]); ?>-儿童价<?php echo ($vo["child_actual_cost"]); ?>[删除]</span><?php endforeach; endif; else: echo "" ;endif; ?>
                            <!-- <span  class="label label-success scenic_price_id" data-id="1">镜花缘[删除]</span>
                             <span  class="label label-success scenic_price_id" data-id="1">万绿湖[删除]</span>-->
                        </p>
                    </div>
                </div>
                <!--景区选择结束-->
            <hr>
                <!--选择保险-->
                <div class="row row-div">
                    <div class="col-md-2" ><p class="text-right"><font color='red'>*</font>可选保险列表:</p></div>
                    <div class="col-md-10">
                        <span class="btn btn-xs btn-primary" onclick="choseInsure()">添加保险</span>
                        <input type="hidden" value="<?php echo ($lineInfo["insure_price_id"]); ?>" id="insure_price_id" name="insure_price_id">
                        <p class="text-primary"> 已选择的保险:</p>
                        <p id="insure_price_id_name">
                            <?php if(is_array($checkedInsure)): $i = 0; $__LIST__ = $checkedInsure;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><p>
                        <span  class="text-warning " data-id="<?php echo ($vo["insure_priceId"]); ?>"><?php echo ($vo["supplier_name"]); ?>(<?php echo ($vo["money_type"]); ?>)-<?php echo ($vo["insure_name"]); ?>-成人价<?php echo ($vo["adult_actual_cost"]); ?>-儿童价<?php echo ($vo["child_actual_cost"]); ?></span>
                                    <span class="text-info">销售成人价</span>:<input type="text" name="insure[<?php echo ($vo["insure_priceId"]); ?>][now_adult_price]" value="<?php echo ($vo["now_adult_price"]); ?>"  onkeyup="clearNoNum(this)" onafterpaste="clearNoNum(this)">
                                      &nbsp; &nbsp;<span class="text-info">销售儿童价</span>:<input type="text" name="insure[<?php echo ($vo["insure_priceId"]); ?>][now_child_price]" value="<?php echo ($vo["now_child_price"]); ?>"  onkeyup="clearNoNum(this)" onafterpaste="clearNoNum(this)"> <span  class="text-warning insure_price_id" data-id="<?php echo ($vo["insure_priceId"]); ?>">[删除]</span>
                                </p><?php endforeach; endif; else: echo "" ;endif; ?>
                        </p>
                    </div>
                </div>
                <!--保险选择结束-->
                <div class="row text-center">
                    <input type="submit"  name="" class="btn btn-primary" value="保存 进入下一步" />
                </div>
                </form>
            </div>
        </div>
    </div>

<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><!-- <![endif]--><!--[if IE]>
<script src="/Public/statics/js/jquery-1.10.2.min.js"></script><![endif]--><!--[if !IE]> -->
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-2.0.3.min.js'>" + "<" + "script>");
</script><!-- <![endif]--><!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='/Public/statics/aceadmin/js/jquery-1.10.2.min.js'>" + "<" + "script>");
</script><![endif]-->
<script type="text/javascript">
    if ("ontouchend" in document) document.write("<script src='/Public/statics/aceadmin/js/jquery.mobile.custom.min.js'>" + "<" + "script>");
</script>
<script src="/Public/statics/aceadmin/js/bootstrap.min.js"></script>
<script src="/Public/statics/aceadmin/js/typeahead-bs2.min.js"></script>
<!--[if lte IE 8]>
<script src="/Public/statics/aceadmin/js/excanvas.min.js"></script><![endif]-->
<script src="/Public/statics/aceadmin/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.ui.touch-punch.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.slimscroll.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.easy-pie-chart.min.js"></script>
<script src="/Public/statics/aceadmin/js/jquery.sparkline.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.pie.min.js"></script>
<script src="/Public/statics/aceadmin/js/flot/jquery.flot.resize.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace-elements.min.js"></script>
<script src="/Public/statics/aceadmin/js/ace.min.js"></script>
<script src="/Public/statics/Operator/js/base.js"></script>
<script src="/Public/statics/layer/layer.js"></script>
<!-- <script src="/Public/js/base.js"></script> -->

    <!-- 表单提交前验证 -->
    <script type="text/javascript" src="/Public/statics/layer/layer.js"></script>
    <script src="/Public/statics/layui/layui.all.js"></script>
    <script src="/Public/statics/layui/layui.js"></script>
    <script src="/Public/statics/Operator/js/line.js"></script>

<script>
    var publicurl="/Public";
    var domainURL="";

    $(function () {
        var bodyH=$(document).height();
        try{
            //parent.resetFrameHeight(bodyH);
        }catch (err){

        }

    })
</script>
</body>
</html>