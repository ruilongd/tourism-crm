
	$(".start").bind('change',function(){
		var mold = 'go';
		search(mold);
		$selval = $(this)[0].value;
		if($selval < 1 || $selval == '' || $selval == null || $selval == undefined){

			return false;
		}
		//根据当前id查询下级属性的json数组集合
		$('.start_level2').remove();
		$(".start_level3").remove();
		$(".start_level4").remove();
		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
              
                	var select =   '<select name="start_level2" class="start_level2" >';
                        select +=  '<option value="-1">请选择省份</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#start").append(select);		
                		
                }

        });
	});

	$('body').on('change','.start_level2',function(){
							mold = 'go';
							search(mold);
							//清除同级的城市
							$(".start_level3").remove();
							$(".start_level4").remove();
                			$selval = $(this)[0].value;
                			$.ajax({
					                url:$linkage,
					                dataType:'json',
					                type:'post',
					                cache:false,
					                data:{id:$selval},
					                success:function(ids){
					                	
					                	if(ids != null && ids != undefined && ids != ''){

					                		var select =   '<select name="start_level3" class="start_level3" >';
					                        select +=  '<option value="-1">请选择城市</option>';
					                        for(var vo in ids){
					                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
					                        }

											select +=  '</select>';
											$("#start").append(select);

						                	}

						                		
						                }

						        });


	});

	$('body').on('change','.start_level3',function(){
						mold = 'go';
						search(mold);
						//清除同级的地区
						$(".start_level4").remove();
            			$selval = $(this)[0].value;
            			$.ajax({
				                url:$linkage,
				                dataType:'json',
				                type:'post',
				                cache:false,
				                data:{id:$selval},
				                success:function(ids){
				       
				                	if(ids != null && ids != undefined && ids != ''){

				                		var select =   '<select name="start_level4" class="start_level4" >';
				                        select +=  '<option value="-1">请选择地区</option>';
				                        for(var vo in ids){
				                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
				                        }

										select +=  '</select>';
										$("#start").append(select);
										$("select[name='start_level4']").on('change',function(ids){
											search(mold);
										});	

				                	}

				                		
				                }

				        });

	});

	//终点
	$(".end").bind('change',function(){
		var mold = 'go';
		search(mold);
		$selval = $(this)[0].value;
		if($selval < 1 || $selval == '' || $selval == null || $selval == undefined){

			return false;
		}
		//根据当前id查询下级属性的json数组集合
		$('.end_level2').remove();
		$(".end_level3").remove();
		$(".end_level4").remove();
		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
                
                	var select =   '<select name="end_level2" class="end_level2" >';
                        select +=  '<option value="-1">请选择省份</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#end").append(select);		
                }

        });
	});

	$('body').on('change','.end_level2',function(){

			search();
			//清除同级的城市
	
			$(".end_level3").remove();
			$(".end_level4").remove();
			$selval = $(this)[0].value;
			$.ajax({
	                url:$linkage,
	                dataType:'json',
	                type:'post',
	                cache:false,
	                data:{id:$selval},
	                success:function(ids){
	                
	                	if(ids != null && ids != undefined && ids != ''){
	                	var select =   '<select name="end_level3" class="end_level3" >';
	                        select +=  '<option value="-1">请选择城市</option>';
	                        for(var vo in ids){
	                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
	                        }

							select +=  '</select>';
							$("#end").append(select);

						}	
	                		
	                }

	        });


	});

	$('body').on('change','.end_level3',function(){

		mold = 'go';
		search(mold);
		//清除同级的城市
		$(".end_level4").remove();
		
		$selval = $(this)[0].value;
		$.ajax({
	            url:$linkage,
	            dataType:'json',
	            type:'post',
	            cache:false,
	            data:{id:$selval},
	            success:function(ids){
	            
	            	if(ids != null && ids != undefined && ids != ''){
	            	var select =   '<select name="end_level4" class="end_level4" >';
	                    select +=  '<option value="-1">请选择地区</option>';
	                    for(var vo in ids){
	                    	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
	                    }

						select +=  '</select>';
						$("#end").append(select);
						$("select[name='end_level4']").on('change',function(ids){
							search(mold);
						});	
					}	
	            		
	            }

	    });

	});


	//回目的地国家点击
	$(".back_start").bind('change',function(){
		var mold = 'back';
		search(mold);
		$selval = $(this)[0].value;
		if($selval < 1 || $selval == '' || $selval == null || $selval == undefined){

			return false;
		}
		//根据当前id查询下级属性的json数组集合
		$('.back_start_level2').remove();
		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
                	console.info(ids);
                	var select =   '<select name="back_start_level2" class="back_start_level2" >';
                        select +=  '<option value="-1">请选择省份</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#back_start").append(select);	
                		
                }

        });
	});

	// 回省份点击
	$("body").on('change','.back_start_level2',function(){
				search();
				//清除同级的城市
				$(".back_start_level3").remove();
				$(".back_start_level4").remove();
    			$selval = $(this)[0].value;
    			$.ajax({
		                url:$linkage,
		                dataType:'json',
		                type:'post',
		                cache:false,
		                data:{id:$selval},
		                success:function(ids){
		                	
		                	if(ids != null && ids != undefined && ids != ''){

		                		var select =   '<select name="back_start_level3" class="back_start_level3" >';
		                        select +=  '<option value="-1">请选择城市</option>';
		                        for(var vo in ids){
		                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
		                        }

								select +=  '</select>';
								$("#back_start").append(select);



			                	}

			                		
			                }

			        });

	});
	//城市点击
	$('body').on('change','.back_start_level3',function(){
		search('back');
		//清除同级的地区
		$(".back_start_level4").remove();
		$selval = $(this)[0].value;
		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
                
                	if(ids != null && ids != undefined && ids != ''){

                		var select =   '<select name="back_start_level4" class="back_start_level4" >';
                        select +=  '<option value="-1">请选择地区</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#back_start").append(select);

                		$("select[name='back_start_level4']").on('change',function(ids){
							search(mold);
						});	

                	}


                		
                }

        });
	});

	//回终点-国家点击
	$("body").on('change','.back_end',function(){

		var mold = 'back';
		$selval = $(this)[0].value;
		if($selval < 1 || $selval == '' || $selval == null || $selval == undefined){

			return false;
		}
		//根据当前id查询下级属性的json数组集合
		$('.back_end_level2').remove();
		$('.back_end_level3').remove();
		$('.back_end_level4').remove();
		$('.back_end_level2').val(0);
		$('.back_end_level3').val(0);
		$('.back_end_level4').val(0);
		console.log($('.back_end_level2').length);
		search(mold);

		$.ajax({
                url:$linkage,
                dataType:'json',
                type:'post',
                cache:false,
                data:{id:$selval},
                success:function(ids){
         				var select = '';
                		select =   '<select name="back_end_level2" class="back_end_level2" >';
                        select +=  '<option value="-1">请选择省份</option>';
                        for(var vo in ids){
                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
                        }

						select +=  '</select>';
						$("#back_end").append(select);	
                		
                }

        });
	});
// 回去-省份点击
	$('body').on('change','.back_end_level2',function(){
			mold = 'back';
			search(mold);
			//清除同级的城市
			$(".back_end_level3").remove();
			$(".back_end_level4").remove();
			$selval = $(this)[0].value;
			$.ajax({
	                url:$linkage,
	                dataType:'json',
	                type:'post',
	                cache:false,
	                data:{id:$selval},
	                success:function(ids){
	         
	                	if(ids != null && ids != undefined && ids != ''){
	                		var select = '';
	                		select =   '<select name="back_end_level3" class="back_end_level3" >';
	                        select +=  '<option value="-1">请选择城市</option>';
	                        for(var vo in ids){
	                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
	                        }

							select +=  '</select>';
							$("#back_end").append(select);

						}	
	                		
	                }

	        });

	});

	$("body").on('change','.back_end_level3',function(){

				mold = 'back';
				search(mold);
				//清除下级的
				$(".back_end_level4").remove();
    			$selval = $(this)[0].value;
    			$.ajax({
		                url:$linkage,
		                dataType:'json',
		                type:'post',
		                cache:false,
		                data:{id:$selval},
		                success:function(ids){
		         
		                	if(ids != null && ids != undefined && ids != ''){
		                		var select = '';
		                		select =   '<select name="back_end_level4" class="back_end_level4" >';
		                        select +=  '<option value="-1">请选择地区</option>';
		                        for(var vo in ids){
		                        	select +=  '<option value="'+ids[vo]['areaId']+'">'+ids[vo]['areaName']+'</option>';
		                        }

								select +=  '</select>';
								$("#back_end").append(select);
								$("select[name='back_end_level4']").on('change',function(ids){
									search(mold);
								});	

							}	
		                		
		                }

		        });
	});

	//固定函数，每次选择下拉点击事件就获取表单相关字段传递给控制器处理获取对应的数据
    function search(mold,kind){
    	console.info(mold,kind);
    	console.info('调用search函数');
    	// console.info(mold);
    	//如果参数mold为go就是去，back就是回
    	
    	$(".suppliers-"+mold).show();
    	$(".seats-"+mold).show();
    	if(mold == 'go'){
    		var go_guo = $("select[name='start_level1']").val() > 0?$("select[name='start_level1']").val():0;
		    var go_sheng = $("select[name='start_level2']").val()>0?$("select[name='start_level2']").val():0;
		    var go_shi = $("select[name='start_level3']").val()>0?$("select[name='start_level3']").val():0;
		    var go_dq =  $("select[name='start_level4']").val()>0?$("select[name='start_level4']").val():0;
		    var bk_guo = $("select[name='end_level1']").val()>0?$("select[name='end_level1']").val():0;
		    var bk_sheng = $("select[name='end_level2']").val()>0?$("select[name='end_level2']").val():0;
		    var bk_shi = $("select[name='end_level3']").val()>0?$("select[name='end_level3']").val():0;
		    var bk_dq  = $("select[name='end_level4']").val()>0?$("select[name='end_level4']").val():0;

    	}else{
    		var go_guo = $("select[name='back_start_level1']").val() > 0?$("select[name='back_start_level1']").val():0;
		    var go_sheng = $("select[name='back_start_level2']").val()>0?$("select[name='back_start_level2']").val():0;
		    var go_shi = $("select[name='back_start_level3']").val()>0?$("select[name='back_start_level3']").val():0;
		    var go_dq =  $("select[name='back_start_level4']").val()>0?$("select[name='back_start_level4']").val():0;
		    var bk_guo = $("select[name='back_end_level1']").val()>0?$("select[name='back_end_level1']").val():0;
		    var bk_sheng = $("select[name='back_end_level2']").val()>0?$("select[name='back_end_level2']").val():0;
		    var bk_shi = $("select[name='back_end_level3']").val()>0?$("select[name='back_end_level3']").val():0;
		    var bk_dq  = $("select[name='back_end_level4']").val()>0?$("select[name='back_end_level4']").val():0;

    	}

    	var go_tf_id = $("select[name='go_traffic_id']").val()>0?$("select[name='go_traffic_id']").val():0; 
    	var bk_tf_id = $("select[name='back_traffic_id']").val()>0?$("select[name='back_traffic_id']").val():0; 
    	var go_sp_id = $("select[name='go_supplier_id']").val()>0?$("select[name='go_supplier_id']").val():0;
    	var bk_sp_id = $("select[name='back_supplier_id']").val()>0?$("select[name='back_supplier_id']").val():0;

    	//出发-返回的地区选择都必须要精确到城市级别
    	console.log(bk_guo,bk_sheng,bk_shi);
		if((go_guo > 0 && go_sheng >0 && go_shi >0) && (bk_guo > 0 && bk_sheng >0 && bk_shi >0)){
			console.info('展示');
			$(".traffics-"+mold).show();
		}else{
			console.info('隐藏');
			$(".traffics-"+mold).hide();
		}
    

	   	$.ajax({
            url:$search,
            dataType:"json",
            type:'POST',
            cache:false,
            data:{go_guo:go_guo,go_sheng:go_sheng,go_shi:go_shi,go_dq:go_dq,bk_guo:bk_guo,bk_sheng,bk_sheng,bk_shi:bk_shi,bk_dq:bk_dq,go_tf_id:go_tf_id,bk_tf_id:bk_tf_id,go_sp_id:go_sp_id,bk_sp_id:bk_sp_id},
            success: function(rs) {
 				// console.info(rs);
            	if(kind == 'traffics'){

            		$("option[class='suppliers_op']").remove();	
            	}else if(kind == 'suppliers'){
					$("option[class='seats_op']").remove();
            	}else{
            		$("option[class='traffics_op']").remove();
            	}

				if(rs != '' && rs != null && rs != undefined){

	            	console.log(rs);
	            	//data供应商
	            	data = rs['suppliers'];
	            	var op = '';
	            	for(var vo in data){
	  
	            		op += '<option class="suppliers_op" value="'+data[vo]['supplier_id']+'">'+data[vo]['supplier_name']+'</option>';

	            	}

	            	//data2交通方式
	            	data2 = rs['traffics'];
	            	var op2 = '';
	            	for(var v in data2){
	  
	            		op2 += '<option class="traffics_op" value="'+data2[v]['traffic_id']+'">'+data2[v]['traffic_name']+'</option>';

	            	}

	            	data3 = rs['seats'];
	            	var op3 = '';
	            	for(var t in data3){
	  
	            		op3 += '<option class="seats_op" value="'+data3[t]['seat_id']+'">'+data3[t]['seat_name']+'</option>';

	            	}


	            	if(kind == 'traffics'){
	            		console.info(op3);
	            		$(".suppliers_"+mold).append(op);
	            		$(".seats_"+mold).append(op3);
	            	}else if(kind == 'suppliers'){
	 
	            		$(".traffics_"+mold).append(op2);
	            		$(".seats_"+mold).append(op3);
	            	}else{
	            		$(".traffics_"+mold).append(op2);
	            		$(".suppliers_"+mold).append(op);
	            		$(".seats_"+mold).append(op3);
	            	}	

            	}
        
            }
    	});
    }

    // $('body').on('change','#traffics_go',function(){
    // 	search('go','traffics');
    // });


    // $('body').on('change','#traffics_back',function(){
    // 	search('back','traffics');
    // });


// 去交通方式
    $("body").on('change','.traffics_go',function(){
    	console.info('去的交通方式选中');
    	$('#traffic-add').modal('show');
    	
    
		var go_guo = $("select[name='start_level1']").val() > 0?$("select[name='start_level1']").val():0;
	    var go_sheng = $("select[name='start_level2']").val()>0?$("select[name='start_level2']").val():0;
	    var go_shi = $("select[name='start_level3']").val()>0?$("select[name='start_level3']").val():0;
	    var go_dq =  $("select[name='start_level4']").val()>0?$("select[name='start_level4']").val():0;
	    var bk_guo = $("select[name='end_level1']").val()>0?$("select[name='end_level1']").val():0;
	    var bk_sheng = $("select[name='end_level2']").val()>0?$("select[name='end_level2']").val():0;
	    var bk_shi = $("select[name='end_level3']").val()>0?$("select[name='end_level3']").val():0;
	    var bk_dq  = $("select[name='end_level4']").val()>0?$("select[name='end_level4']").val():0;
    	var go_tf_id = $("select[name='go_traffic_id']").val()>0?$("select[name='go_traffic_id']").val():0; 
    	var bk_tf_id = $("select[name='back_traffic_id']").val()>0?$("select[name='back_traffic_id']").val():0; 
    	var go_sp_id = $("select[name='go_supplier_id']").val()>0?$("select[name='go_supplier_id']").val():0;
    	var bk_sp_id = $("select[name='back_supplier_id']").val()>0?$("select[name='back_supplier_id']").val():0;

    	$.ajax({
            url:$search,
            dataType:"json",
            type:'POST',
            cache:false,
            data:{type:'go',go_guo:go_guo,go_sheng:go_sheng,go_shi:go_shi,go_dq:go_dq,bk_guo:bk_guo,bk_sheng,bk_sheng,bk_shi:bk_shi,bk_dq:bk_dq,go_tf_id:go_tf_id,bk_tf_id:bk_tf_id,go_sp_id:go_sp_id,bk_sp_id:bk_sp_id},
            success: function(data) {
            	console.info(data);
            	//组装数据
            	$("#tr_traffic").empty();
            		var tr = '';
            		for(var i in data['suppliers']){
            			tr += '<tr>';
            			tr += '<td style="text-align: center;"><span>'+data['suppliers'][i]['supplier_name']+'</th>';
            			tr += '<td style="text-align: center;">';
            			for(var j in data['suppliers'][i]['seats']){
            				

            				if(j < 1){
            					tr += '<input name="traffic_priceId"  type="radio" checked value="'+data['suppliers'][i]['seats'][j]['traffic_priceId']+'"/><span id="room-'+data['suppliers'][i]['seats'][j]['traffic_priceId']+'">'+data['suppliers'][i]['seats'][j]['seat_name']+'</span>';

            				}else{
            					tr += '<input name="traffic_priceId" type="radio"  value="'+data['suppliers'][i]['seats'][j]['traffic_priceId']+'"/><span id="room-'+data['suppliers'][i]['seats'][j]['traffic_priceId']+'">'+data['suppliers'][i]['seats'][j]['seat_name']+'</span>';
            				}	
            			}
            			
            			tr += '</td> ';
            			tr += '</tr>';
            		}
            	$("#tr_traffic").append(tr);		
            	$('#traffic-add').modal('show');
            }
        });    

    });	

    $('body').on('click','.add_traffic',function(){

    	//交通方式资料
						$seat_id = $("input[name='traffic_priceId']:checked").val();
						$traffic_name = $("input[name='traffic_priceId']:checked").parent('td').prev('td').find('span').text();
						$seat_name = $("#room-"+$seat_id).text();

						if($seat_id > 0 && $seat_id != undefined){

							var tr = '<tr style="line-height: 12px;">';
							tr += '<input name="go_price_id[]" type="hidden" value="'+$seat_id+'" />';
							tr += '<td class="mytd">'+$traffic_name+'</td>';
							tr += '<td class="mytd">'+$seat_name+'</td>';
							tr += '<td class="mytd"><input type="button" class="btn btn-danger deltr" value="删除"/></td>';
							tr += '</tr>';
							//组装数据生成li展示到页面上
							$("#go_traffics_list").append(tr);

						}else{

							layer.msg('必须选中一个交通方式-座位！');return false;
						}

						$('#traffic-add').modal('hide');


    });

    // 回交通方式
    $("body").on('change','.traffics_back',function(){

    	$('#back_traffic-add').modal('show');
    	
    
		var go_guo = $("select[name='back_start_level1']").val() > 0?$("select[name='back_start_level1']").val():0;
	    var go_sheng = $("select[name='back_start_level2']").val()>0?$("select[name='back_start_level2']").val():0;
	    var go_shi = $("select[name='back_start_level3']").val()>0?$("select[name='back_start_level3']").val():0;
	    var go_dq =  $("select[name='back_start_level4']").val()>0?$("select[name='back_start_level4']").val():0;
	    var bk_guo = $("select[name='back_end_level1']").val()>0?$("select[name='back_end_level1']").val():0;
	    var bk_sheng = $("select[name='back_end_level2']").val()>0?$("select[name='back_end_level2']").val():0;
	    var bk_shi = $("select[name='back_end_level3']").val()>0?$("select[name='back_end_level3']").val():0;
	    var bk_dq  = $("select[name='back_end_level4']").val()>0?$("select[name='back_end_level4']").val():0;
    	var go_tf_id = $("select[name='go_traffic_id']").val()>0?$("select[name='go_traffic_id']").val():0; 
    	var bk_tf_id = $("select[name='back_traffic_id']").val()>0?$("select[name='back_traffic_id']").val():0; 
    	var go_sp_id = $("select[name='go_supplier_id']").val()>0?$("select[name='go_supplier_id']").val():0;
    	var bk_sp_id = $("select[name='back_supplier_id']").val()>0?$("select[name='back_supplier_id']").val():0;

    	$.ajax({
            url:$search,
            dataType:"json",
            type:'POST',
            cache:false,
            data:{type:'back',go_guo:go_guo,go_sheng:go_sheng,go_shi:go_shi,go_dq:go_dq,bk_guo:bk_guo,bk_sheng,bk_sheng,bk_shi:bk_shi,bk_dq:bk_dq,go_tf_id:go_tf_id,bk_tf_id:bk_tf_id,go_sp_id:go_sp_id,bk_sp_id:bk_sp_id},
            success: function(data) {
            	console.info(data);
            	//组装数据
            	$("#tr_traffic_back").empty();
            		var tr = '';
            		for(var i in data['suppliers']){
            			tr += '<tr>';
            			tr += '<td style="text-align: center;"><span>'+data['suppliers'][i]['supplier_name']+'</th>';
            			tr += '<td style="text-align: center;">';
            			for(var j in data['suppliers'][i]['seats']){
            				if(j < 1){
            					tr += '<input name="traffics_priceId" type="radio" checked value="'+data['suppliers'][i]['seats'][j]['traffic_priceId']+'"/><span id="rooms-'+data['suppliers'][i]['seats'][j]['traffic_priceId']+'">'+data['suppliers'][i]['seats'][j]['seat_name']+'</span>';

            				}else{
            					tr += '<input name="traffics_priceId" type="radio"  value="'+data['suppliers'][i]['seats'][j]['traffic_priceId']+'"/><span id="rooms-'+data['suppliers'][i]['seats'][j]['traffic_priceId']+'">'+data['suppliers'][i]['seats'][j]['seat_name']+'</span>';
            				}	
            			}
            			
            			tr += '</td> ';
            			tr += '</tr>';
            		}
            	$("#tr_traffic_back").append(tr);		
            	$('#back_traffic-add').modal('show');
            }
        });    

    });	

    $('body').on('click','.add_traffic_back',function(){

    	//交通方式资料
						$seat_id = $("input[name='traffics_priceId']:checked").val();
						$traffic_name = $("input[name='traffics_priceId']:checked").parent('td').prev('td').find('span').text();
						$seat_name = $("#rooms-"+$seat_id).text();

						if($seat_id > 0 && $seat_id != undefined){

							var tr = '<tr style="line-height: 12px;">';
							tr += '<input name="back_price_id[]" type="hidden" value="'+$seat_id+'" />';
							tr += '<td class="mytd">'+$traffic_name+'</td>';
							tr += '<td class="mytd">'+$seat_name+'</td>';
							tr += '<td class="mytd"><input type="button" class="btn btn-danger deltr" value="删除"/></td>';
							tr += '</tr>';
							//组装数据生成li展示到页面上
							$("#back_traffics_list").append(tr);

						}else{

							layer.msg('必须选中一个交通方式-座位！');return false;
						}

						$('#back_traffic-add').modal('hide');


    });

//删除当前删除按钮的父级tr标签
$('body').on('click','.deltr',function(){

	$(this).parent().parent('tr').remove();

});

