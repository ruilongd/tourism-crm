function checkstep1(){

	if($("select[name='cityId']").val() == '' || $("select[name='cityId']").val() == undefined){
		layer.msg('请选择出发地！');
		return false;
	}

	if($("input[name='destination_id[]']:checked").val() == '' || $("input[name='destination_id[]']:checked").val() == undefined){
		layer.msg('请选择目的地！');
		return false;
	}


	if($("input[name='theme[]']:checked").val() == '' || $("input[name='theme[]']:checked").val() == undefined){
		layer.msg('必须要选择最少一种主题！');
		return false;
	}
	// if($("#start_time").val() == '' || $("#end_time").val() == ''){
	// 	layer.msg('必须要设置开始时间和结束时间！');
	// 	return false;
	// }



	return true;
	
}

