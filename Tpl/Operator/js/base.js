/**
 * Created by Administrator on 2017/9/6.
 */

// $(function () {
//     $(document).ajaxStart(function () {
//         layer.load(1, {
//             shade: [0.1, '#fff'] //0.1透明度的白色背景
//         });
//     });
//     $(document).ajaxStop(function () {
//         setTimeout(function () {
//             layer.closeAll();
//         },1000);

//     });
// });



//ajax上传附件
function ajax_upload_file(file, _this) {
    var formData = new FormData();
    formData.append("file", file[0]);
    $.ajax({
        type: 'POST',
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        processData: false,
        url: '/index.php?m=Operator&c=Base&a=ajax_upload_file',
        dataType: 'json',
        success: function (data) {
            if (data.status==1) {
                $(_this).prev().val(data.path);
                $(_this).next().attr('src', data.path);
            }else{
                layer.msg(data.info);
            }
        }
    });
}

/**
 * 根据省份获取市区
 */
function getCity(_this) {
    var province_id=$(_this).find(':selected').val();
    $("select[name='city_id']").html('<option value="">==选择市==</option>');
    $("select[name='area_id']").html('<option value="">==选择区==</option>');
    $.ajax({
        type: 'POST',
        data: {province_id:province_id},
        url: '/index.php?m=Operator&c=Platform&a=getCity',
        dataType: 'json',
        success: function (data) {
           var html='';
           for(var i in data){
               html+=" <option value='"+data[i].areaId+"'>"+data[i].areaName+"</option>";
           }
           $("select[name='city_id']").append(html);
        }
    });
}

/**
 * 根据市获取区
 */
function getArea(_this) {
    var city_id=$(_this).find(':selected').val();
    $("select[name='area_id']").html('<option value="">==选择区==</option>');
    $.ajax({
        type: 'POST',
        data: {city_id:city_id},
        url: '/index.php?m=Operator&c=Platform&a=getArea',
        dataType: 'json',
        success: function (data) {
            var html='';
            for(var i in data){
                html+=" <option value='"+data[i].areaId+"'>"+data[i].areaName+"</option>";
            }
            $("select[name='area_id']").append(html);
        }
    });
}

/**
 * 更改城市的锁定和启用状态
 */
function changeIsShow(areaId,status) {
    $.ajax({
        type: 'POST',
        data: {areaId:areaId,isShow:status},
        url: '/index.php?m=Operator&c=City&a=changeIsShow',
        dataType: 'json',
        success: function (data) {
            layer.msg(data.msg);
            if(data.status==1){
                setTimeout(function(){
                    location.reload();
                },1500);
            }
        }
    });
}


/**
 * 限制只能输入整数或者带2位小数的
 */

function clearNoNum(obj)
{
    //先把非数字的都替换掉，除了数字和.
    obj.value = obj.value.replace(/[^\d.]/g,"");
    //必须保证第一个为数字而不是.
    obj.value = obj.value.replace(/^\./g,"");
    //保证只有出现一个.而没有多个.
    obj.value = obj.value.replace(/\.{2,}/g,".");
    //保证.只出现一次，而不能出现两次以上
    obj.value = obj.value.replace(".","$#$").replace(/\./g,"").replace("$#$",".");
}