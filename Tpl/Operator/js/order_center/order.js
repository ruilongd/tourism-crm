var $fee = $('#fee'),
    $totalFee = $('#totalFee'),
    $visa = $('#visa'),
    currentDay = '';
/**
 * 打开行程详情
 * @param int id
 */
function openDetail(id){
    if($('#calendar .date-checked').length<1){
        layer.alert('请选择出团日期', {icon: 5});
        return;
    }

    var str = '';
    $.each($('#calendar .date-checked').data(),function(i,n){
        str += ('/' + i + '/' + n);
    })
    // console.log('/Operator/OrderCenter/tripDetail/id/'+id+str);
    layer.open({
        type: 2,
        title: '行程详情',
        content: '/Operator/OrderCenter/tripDetail/id/'+id+str,
        area: ['80%', '80%']
    })
}

//保险
$('#product .insure').on('click', function(){
    var $insure = $('#product .insure:checked');

    //存在被选中的保险
    if($insure.length>0){
        $insure.prop('checked', false);
        $(this).prop('checked', true);
    }
    totalFeeChange();
})

//附加产品变动
$('#product .add_product').on('click', function(){
    var $addProduce = $('#product .add_product');
    totalFeeChange();
})

//线路总费用变动
function totalFeeChange(){
    //附加产品费用
    var addProduce = 0;
    $('#product .add_product:checked').each(function(i,n){
        addProduce += parseFloat($(n).data('price'));
    })
    $totalFee.html( parseFloat($fee.html()) + addProduce + ($('.insure:checked').data('price') || 0) * 1 + ($('#visa:checked').data('price') || 0) * 1  );
}

//签证勾选变动
$visa.on('change', function(){
    totalFeeChange();
})

function checkForm(){
    //出团日期
    var $el = $('#calendar').find('.date-checked');
    //如果没有选择日期,不能提交
    if($el.length<1){
        layer.msg('请选择出团日期');
        return false;
    }
    var index = layer.load(1, {
      shade: [0.5,'#000'] //0.1透明度的白色背景
    });
    var flag = 0;
    $.ajax({
        url: '/Operator/OrderCenter/checkGroup',
        async: false,
        dataType: 'json',
        data: { day: $el.data('day'), line_id:  $('#lineId').val() },
        success: function(data){
             if(data.status == 1)    flag = 1;
             else layer.alert(data.msg, {icon: 5});
        }
    })

    layer.close(index);
    if(flag == 0) return false;

    var $form = $('#form');
    var html = '<input type="hidden" name="day" value="'+$el.data('day')+'"/>';
    if($el.data('sprice_id')){
        html += '<input type="hidden" name="sprice_id" value="'+$el.data('sprice_id')+'"/>';
    }
    $form.append(html);
    return true;
}

