
/**
 * 选中搜索目的地列表中的项目
 */
$('#destResult').on('click', 'li', function(){
    $(this).addClass('actives').siblings().removeClass('actives');
    $('#destination').val($(this).html());
    $('#dataList').hide();
})

//显示/隐藏搜索列表
$('#showList').on('click', function(){
    $('#dataList').toggle();
})


//鼠标滑过事件
$('#aside .check').hover(function(){
    $(this).addClass('active').siblings().removeClass('active');
},function(){
    $(this).removeClass('active');
})

//点击左边菜单’全部‘按钮
$('.checkli').on('click', function(){
    $(this).addClass('checked').siblings().removeClass('checked');
})

//点击省内游目的地选择中的地址标签
$('#inProvice').on('click', 'dd a', function(){
    choosePlace(this,'city-active');
})

//点击国内游中的目的地标签
$('#inCountry').on('click', 'dt a', function(){
    choosePlace(this,'provice-active');
})

//点击国内游中的目的地标签
$('#inWorld').on('click', 'dt a', function(){
    choosePlace(this,'country-active');
})

/**
 * 左侧菜单地址选择回调函数
 * @param  {[obj]} obj   [被点击的元素]
 * @param  {[string]} str [激活的类]
 */
function choosePlace(obj, str){
    $(obj).closest('.panel').find('.' + str).removeClass( str);
    $(obj).addClass(str);
    var $li = $(obj).closest('li');
    $li.addClass('checked').siblings().removeClass('checked');
    $li.children('.col-xs-8').html($(obj).html());
    $('#destination').val($(obj).html());
}

/**
 * 点击左侧菜单的搜索按钮
 */
$('#submitDest').on('click',function(){
    var $inputDest = $('#inputDest');
    if(!$inputDest.val()){
        layer.msg('请输入搜索内容');
        $inputDest.focus();
    }
    $.getJSON('/Reseller/OrderCenter/getCity',{city:$inputDest.val()}, function(data){
        if(!data){
            $('#destResult').html('');
            return false;
        }
        var html = '';
        $.each(data, function(i,n){
            html += '<li>'+n['areaName']+'</li>';
        })
        $('#destResult').html(html);
    })
})


/**
 * 线路查询
 */
getLine(0);
function getLine(page){
    var param = {};
    param['city'] = $('#destination').val();
    param['day'] = $('input[name="day"]:checked').val();
    param['min_price'] = $('#minPrice').val();
    param['max_price'] = $('#maxPrice').val();
    param['set_out'] = $('#setOut').val();
    param['arrive'] = $('#arrive').val();
    param['line_sn'] = $('#line_sn').val();
    param['line_name'] = $('#keyWord').val();
    if(page) param['p'] = page;
    // var index = layer.load(0, {shade: true});

    $.getJSON(
        '/Reseller/OrderCenter/getLine',
        param,
        function(data){

            if(!data['root']){
                $('#line').html('');
                return false;
            }
            var html = '';

            $.each(data['root'], function(i,n){
                html += '<tr>\
                    <td>'+n['line_sn']+'</td>\
                    <td>'+n['line_name']+n['source_type']+'</td>\
                    <td>'+n['origin']+'</td>\
                    <td>'+n['destination_name']+'</td>\
                    <td>'+n['travel_days']+'</td>\
                    <td>成人价：'+n['adult_price']+'<br/>儿童价：'+n['child_price']+'</td>\
                    <td><a href="/Reseller/OrderCenter/order/id/'+n['line_id']+'" class="btn btn-primary">预订</a></td>\
                </tr>';
            })
            $('#pager').html(data.pager);
            $('#line').html(html);
    });
}

/**
 * 查看线路还能容纳多少游客
 */
function getLineBook(line_id){
    $.getJSON('/Reseller/OrderCenter/isBook/line_id/'+line_id,function(data){
        if(data['status'] == -1){
            layer.msg(data['msg']);
        }else{
            layer.confirm(data['msg'], {
              btn: ['进入预订','取消'] //按钮
            }, function(){
              window.location = '/Reseller/OrderCenter/order/id/'+ line_id;
            });

        }

    })
}


/**
 * ajax翻页
 */
$('body').on('click', '.ajax-page', function(){
    getLine($(this).data('page'));
})