<?php
/**
 * 获取验证码
 */
function validateCode(){
    ob_clean();
    $config =    array(
        'imageW'    =>    160,    // 验证码字体大小
        'imageH'    =>    40,    // 验证码字体大小
        'fontSize'    =>    18,    // 验证码字体大小
        'length'      =>    5,     // 验证码位数
        'useNoise'    =>    false, // 关闭验证码杂点
    );
    $Verify = new \Think\Verify($config);
    $Verify->entry();
}

/**
 * 判断是否拥有权限
 */
function checkAuth($mca){
    if(!$mca){
        $mca=MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;
    }
    $auth=new \Think\Auth();
    $result=$auth->check($mca,session('operator_user.operator_id'));
    return $result;
}