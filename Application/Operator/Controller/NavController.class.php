<?php
namespace Operator\Controller;
/**
 * 后台菜单管理
 */
class NavController extends BaseController{

    public function __construct(){
        parent::__construct();
    }


	/**
	 * 菜单列表
	 */
	public function index(){

	    $data=D('AdminNav')->getTreeData('tree','order_number,id');
		$assign=array(
			'data'=>$data
			);
		$this->assign($assign);
		$this->display();
	}

	/**
	 * 添加菜单
	 */
	public function add(){
		$this->error('关注商业版');
	}

	/**
	 * 修改菜单
	 */
	public function edit(){
		$this->error('关注商业版');
	}

	/**
	 * 删除菜单
	 */
	public function delete(){
		$this->error('关注商业版');
	}

	/**
	 * 菜单排序
	 */
	public function order(){
		$this->error('关注商业版');
	}


}
