<?php
namespace Operator\Controller;
use Think\Controller;
/**
 * Base基类控制器
 */
class BaseController extends Controller{
    protected  $admin_id;
    /**
     * 初始化方法
     */
    public function __construct(){
        parent::__construct();

        //A('Operator/Closing')->unitProductExpire();
        //币种列表
        $currency=S('currency');

        if(!$currency){
            $currencyList=M('currency')->select();
            $tempCurrency=[];
            foreach ($currencyList as $k=>$v){
                $tempCurrency[$v['currency_id']]=$v['currency_name'];
                $tempCurrency[$v['currency_id'].'_sym']=$v['currency_symbol'];
            }
            $currency=$tempCurrency;
            S('currency',$tempCurrency,3600);
        }
        $this->assign('currency',$currency);


        //超管资料
        $operatorInfo=S('operatorInfo');
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        if(!$operatorInfo){
            $supplierInfo=M('operator')->where(array('operator_id'=>$admin_id))->find();
            $supplierInfo['currency']=$currency[$supplierInfo['currency_id']];
            S('operatorInfo',$supplierInfo,3600);
        }

        $this->assign('operatorInfo',$operatorInfo);



        $auth=new \Think\Auth();
        $rule_name=MODULE_NAME.'/'.CONTROLLER_NAME.'/'.ACTION_NAME;
        //不需要验证的控制器方法列表
        $notValidate=C('NOT_VALIDATE');


        //登录验证
        if(!session('operator_user')){
            header('Location:'.U('Operator/Login/login'));
            exit();
        }

        //超级管理员ID
        $userInfo=session('operator_user');
        // dump($userInfo);die;
        if($userInfo['pid']==0){
            $this->admin_id=$userInfo['operator_id'];
        }else{
            $this->admin_id=$userInfo['pid'];
        }

        //登录控制器不用验证
        if(!in_array($rule_name,$notValidate)){
            // dump($rule_name);
            // dump($admin_id);
            $result=$auth->check($rule_name,session('operator_user.operator_id'));
            // dump($result);die;
            if(!$result){
                die('您没有权限访问');
                $this->error('您没有权限访问',U('Operator/Index/index'));
            }
        }

        // 分配菜单数据  'platform'=>2 在模型里面配置一下参数 1运营商,2经销商,3分销
        $nav_data=D('AdminNav')->getTreeData('level','order_number,id');
        $assign=array(
            'nav_data'=>$nav_data
        );
        $this->assign($assign);

    }

    /**
     * 用于数据展示
     * @param  [array] $data [description]
     * @param  [array] $exclude [不需展示的数据的键名]
     */
    public function dataToDisplay($data, $exclude=''){
        if(is_array($exclude))
            foreach($exclude as $v)
                unset($data[$v]);

        foreach ($data as $k => $v)
            $this->$k = $v;

    }

    public  function getCurrency(){
        $currency=S('currency');
        $operatorInfo=session('operator_user');
        return $currency[$operatorInfo['currency_id']];
    }

    /**
     * ajax上传文件
     */
    public function ajax_upload_file()
    {
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     31457280 ;// 设置附件上传大小
        $upload->exts     	=     array('jpg', 'gif', 'png', 'jpeg','doc','docx','xlsx','pdf','wps','txt','xls');// 设置附件上传类型
        $upload->rootPath	= 	  './Upload';
        $upload->savePath  =      '/business/'; // 设置附件上传目录
        // 上传文件
        $info   =   $upload->upload();
        if(!$info)
        {
            // 上传错误提示错误信息
            $this->error($upload->getError());
        }else{
            $path='/Upload'.$info['file']['savepath'].$info['file']['savename'];
            $this->ajaxReturn(['path'=>$path,'name'=>$info['file']['name'],'status'=>1]);
        }
    }


    /**
     * 运营商写操作日志
     */
    public  function  oplog($content){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $data['log_content']=$content;
        $data['log_time']=time();
        $data['log_ip']=get_client_ip();
        $data['inputer']=session('operator_user.operator_id');
        $data['inputer_name']=session('operator_user.operator_account').'|'.session('operator_user.operator_name');
        $data['operator_id']=$admin_id;
        M('oplog')->add($data);
    }



}
