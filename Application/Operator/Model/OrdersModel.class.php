<?php
namespace Operator\Model;
/**
 * 订单
 */
class OrdersModel extends BaseModel{

    protected $tableName = 'line_orders';

    /**
     * 订单详情
     */
    public  function  detail($id){

        $field="o.order_remarks,o.order_id,o.order_num,o.tourists_ids,o.contact_id,l.group_num,l.line_name,o.adult_num,o.child_num,o.oldMan_num,o.create_time,o.out_time,o.order_status,op.operator_name,tou.tourists_name,tou.tourists_phone";

        $list=M('line_orders as o')
            ->join("LEFT JOIN __PUBLIC_LINE__ as l on l.line_id=o.line_id")
            ->join("LEFT JOIN __OPERATOR__ as op on l.operator_id=op.operator_id")
            ->join("LEFT JOIN __PUBLIC_TOURISTS__ as tou on tou.tourists_id=o.contact_id")
            ->field($field)
            ->where(['o.order_id'=>$id])
            ->find();

        $list['create_time']=date('Y-m-d',$list['create_time']);
        $list['out_time']=date('Y-m-d',$list['out_time']);
        $list['order_status']=self::orderStatus($list['order_status']);
        $list['headcount']="成".$list['adult_num']."人;儿".$list['child_num']."人";

        //发票信息
        $invoice=M('public_receipt')->where(['order_id'=>$id])->find();

        //游客信息
        $tourist=M('public_tourists')->where(['order_id'=>$id])->select();

        return ['orderInfo'=>$list,'invoice'=>$invoice,'tourist'=>$tourist];


    }


    /**
     * 订单列表
     */
    public  function  orderList(){
        $admin_id=session('operator_user.pid')==0?session('operator_user.operator_id'):session('operator_user.pid');
        $count      = $this->where(['supplier_id'=>$admin_id,'is_delete'=>1])->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=$this->where(['operator_id'=>$admin_id,'is_delete'=>1])->order('visa_id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        return ['list'=>$list,'show'=>$show];
    }

    /**
     * 订单状态
     */
    public  function  orderStatus($status){
        $name='';
        switch ($status){
            case -1:$name='未支付';break;
            case 1:$name='已支付';break;
            case -2:$name='待结算';break;
            case 2:$name='已结算';break;
            case -3:$name='待退款';break;
            case -4:$name='已退款';break;
        }
        return $name;
    }


}

