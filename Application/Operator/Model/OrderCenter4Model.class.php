<?php
namespace Operator\Model;
/**
 * 订购中心游客信息录入
 */
class OrderCenter4Model extends BaseModel{
	public function detailView(){
		$this->oid = I('order_id');
		//订单与团号信息
		$order_info = M('line_orders as o')
			->field('o.*,g.group_num,g.group_status,g.line_name')
			->join('__PUBLIC_GROUP__ as g on o.group_id = g.group_id and o.order_id = '.$this->oid)
			->find();
		$this->order = $order_info;
		//服务 房间酒店交通升级保险附加产品
		$service = $this->service();
		// echo json_encode($service);die;
		return ['order' => $order_info, 'tourist' => $this->tourist(), 'service'=>$service];
	}

	/**
	 * 服务
	 * @return array [<房间酒店交通升级保险附加产品>]
	 */
	private function service(){
		$arr = M('order_unit_detail')
			->field('source_id,order_id,traffic_go_back,source_type,adult_count,child_count,unit_cost,use_date,room_count,room_diff,now_cost,is_upgrade')
			->where(['order_id' => $this->oid, 'source_type'=>['in','1,2,4']])
			->order('use_date asc')
			->select();
		$service = $this->arrangeService($arr);
		//附加产品
		$service['product'] = M('order_addition')
			->field('product_name, now_money')
			->where(['order_id' => $this->oid])
			->select();
		return $service;
	}

	/**
	 * 整理服务数据
	 * @param [array] $[arr] [服务数据]
	 * @return [array] $[arr] [服务数据]
	 */
	private function arrangeService($arr){
		$arrange = [];
		$info = [];
		$out_day = date('Y-m-d', $this->order['out_time']);
		//数据类型判断
		foreach($arr as $v){
			$key = '';
			//类型 1交通,2酒店,3景区,4保险
			switch ($v['source_type']) {
				case 1:
					//可升级
					if($v['is_upgrade']) $key = 'up_traffic';
					else $key = 'traffic';
					break;

				case 2:
					//可升级
					if($v['is_upgrade']) $key = 'up_hotel';
					else $key = 'hotel';
					break;

				// case 3:
				// 	$key = 'scenic';
				// 	break;

				case 4:
					$key = 'insure';
					break;
			}
			$arrange[$key][] = $v;
		}

		//数据按类型整理
		foreach($arrange as $k =>$v){
			switch($k){
				case 'traffic':
					$temp = M('public_single_traffic_price as p')
						->field('traffic_priceId,seat_name')
						->join('__PUBLIC_SINGLE_TRAFFIC_SEAT__ as s on p.seat_id = s.seat_id and traffic_priceId in ('.implode(',', array_map('array_shift', $v)).')')
						->select();
					break;

				case 'up_traffic':
					$temp = M('public_single_traffic_price as p')
						->field('traffic_priceId,seat_name')
						->join('__PUBLIC_SINGLE_TRAFFIC_SEAT__ as s on p.seat_id = s.seat_id and traffic_priceId in ('.implode(',', array_map('array_shift', $v)).')')
						->select();
					break;

				case 'hotel':
					$temp = M('public_single_room_price as p')
						->field('room_priceId,room_name,supplier_name')
						->join('__PUBLIC_SINGLE_ROOM__ as r on p.room_id = r.room_id and room_priceId in ('.implode(',', array_map('array_shift', $v)).')')
						->join('__PUBLIC_SINGLE_SUPPLIER__ as s on p.supplier_id = s.supplier_id')
						->select();
					break;

				case 'up_hotel':
					$temp = M('public_single_room_price as p')
						->field('room_priceId,room_name,supplier_name')
						->join('__PUBLIC_SINGLE_ROOM__ as r on p.room_id = r.room_id and room_priceId in ('.implode(',', array_map('array_shift', $v)).')')
						->join('__PUBLIC_SINGLE_SUPPLIER__ as s on p.supplier_id = s.supplier_id')
						->select();
					break;

				// case 'scenic':
				// 	$temp = M('public_single_scenic_price as p')
				// 		->field('traffic_priceId,seat_name')
				// 		->join('__PUBLIC_SINGLE_SCENIC__ as s on p.seat_id = s.seat_id and traffic_priceId in ('.implode(',', array_map('array_shift', $v)).')')
				// 		->select();
				// 	break;

				case 'insure':
					$temp = M('public_single_insure_price as p')
						->field('insure_priceId,insure_name')
						->join('__PUBLIC_SINGLE_INSURE__ as s on p.insure_id = s.insure_id and insure_priceId in ('.implode(',', array_map('array_shift', $v)).')')
						->select();
					break;
			}
			$info[$k] = $this->changeIndex($temp);
		}
		//将处理好的数据拼接到一起
		foreach($arrange as $k => $way){
			foreach($way as $kk=>$vv){
				$arrange[$k][$kk] = array_merge($vv, $info[$k][$vv['source_id']]);
			}
		}

		foreach($arrange['up_hotel'] as $k => $v){
			unset($arrange['up_hotel'][$k]);
			$arrange['up_hotel'][$v['use_date']][] = $v;
		}

		//按日期区分酒店信息
        if(!empty($arrange['hotel'])){
            foreach ($arrange['hotel'] as $k => $v) {
                unset($arrange['hotel'][$k]);
                //如果当天存在酒店升级信息并且是第一次创建当天标准酒店信息 合并
                if (isset($arrange['up_hotel'][$v['use_date']]) && !isset($arrange['hotel'][$v['use_date']])) {
                    $arrange['hotel'][$v['use_date']]['up'] = $arrange['up_hotel'][$v['use_date']];
                }
                $arrange['hotel'][$v['use_date']]['standrand'][] = $v;
            }
        //只存在升级的
        } elseif(!empty($arrange['up_hotel'])){
            //$k 是日期
            foreach ($arrange['up_hotel'] as $k => $v) {
                $arrange['hotel'][$k]['up'] = $arrange['up_hotel'][$k];
            }
        }

		// $arrange['hotel'] = array_merge($arrange['hotel'], $arrange['up_hotel']);
		unset($arrange['up_hotel']);
		//处理交通往返信息
		foreach($arrange['traffic'] as $k =>$v){
			unset($arrange['traffic'][$k]);
			if($v['source_id'] == $this->order['go_traffic_priceId']){
				$arrange['traffic']['go'] = $v;
				continue;
			}
			$arrange['traffic']['back'] = $v;
		}
		$record_day = 0;
		//区分来回的交通升级方式
		foreach($arrange['up_traffic'] as $k => $v){
			unset($arrange['up_traffic'][$k]);
			if($v['traffic_go_back'] == 1){
				$arrange['up_traffic']['go'][] = $v;
			}else{
				$arrange['up_traffic']['back'][] = $v;
			}
		}

		//往返交通方式的名称
		$arrange['go_traffic_name'] = M('public_single_traffic as t')
			->join('__PUBLIC_SINGLE_TRAFFIC_PRICE__ as p on t.traffic_id = p.traffic_id and traffic_priceId = '.$this->order['go_traffic_priceId'])
			->getField('traffic_name');
		$arrange['back_traffic_name'] = M('public_single_traffic as t')
			->join('__PUBLIC_SINGLE_TRAFFIC_PRICE__ as p on t.traffic_id = p.traffic_id and traffic_priceId = '.$this->order['back_traffic_priceId'])
			->getField('traffic_name');

		return $arrange;
	}

	/**
	 * 获取游客信息
	 * @return [array] [游客信息]
	 */
	private function tourist(){
		//游客信息
		$tourist_info = M('public_tourists')
			->where(['order_id' => $this->oid])
			->select();
		foreach($tourist_info as $k => $v){
			$tourist_info[$k]['documents_type'] = '身份证';
			switch ($v['tourists_type']) {
				case 1:
					$tourist_info[$k]['tourists_type'] = '成人';
					break;
				case 2:
					$tourist_info[$k]['tourists_type'] = '老人';
					break;
				case 3:
					$tourist_info[$k]['tourists_type'] = '小孩';
					break;
			}
			if($v['tourists_sex'] == 1){
				$tourist_info[$k]['tourists_sex'] = '男';
			}else{
				$tourist_info[$k]['tourists_sex'] = '女';
			}

			if($v['is_contact'] == 1){
				$tourist_info['contact'] = $tourist_info[$k];
				unset($tourist_info[$k]);
			}
		}
		array_unshift($tourist_info, $tourist_info['contact']);
        unset($tourist_info['contact']);
		return $tourist_info;
	}

	public function orderChange(){
		$p = I('post.');
		$order_id = $p['order_id'];
		$need_pay              = M('line_orders')->where(['order_id' => $order_id])->getField('need_pay');
        $order['end_need_pay'] = $p['end_need_pay'] ? $p['end_need_pay'] : $need_pay;
        $order['close_money']  = $need_pay;
		unset($p['need_pay']);
		unset($p['end_need_pay']);
		M()->startTrans();
		$res = M('line_orders')
			->where(['order_id' => $order_id])
			->save($order);
		if($res === false){
			M()->rollback();
			return ['status' => -1, 'msg' => '修改价格失败'];
		}
		if(!empty($p)){
			$p['operator_id'] = session('operator_user.pid') ? session('operator_user.pid') : session('operator_user.operator_id');
			$p['open_time'] = time();
			$res1 = M('public_receipt')->add($p);
			if($res1 === false){
				M()->rollback();
				return ['status' => -2, 'msg' => '写入发票数据失败'];
			}
		}
		M()->commit();
		return ['status' => 1, 'msg' => '成功'];
	}
}
