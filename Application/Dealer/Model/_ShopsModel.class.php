<?php
namespace Dealer\Model;
/**
 * 商家管理
 */
class ShopsModel extends BaseModel{

    protected $tableName = 'public_single_supplier';

    /**
     * 新增单品供应商
     */
    public  function  addShops($data){
        $admin_id=session('dealer_user.pid')==0?session('dealer_user.supplier_id'):session('dealer_user.pid');
        $data['admin_supplier_id']=$admin_id;
        if($this->create($data)){
            return $this->add();
        }
        return false;
    }


    /**
     * 生成新的商家编号
     */
    public  function shopSn(){
       $supplier_sn= $this->order('supplier_sn desc')->getField('supplier_sn');
       if(!$supplier_sn){
           return "10000".mt_rand(100,999);
       }
       return $supplier_sn+mt_rand(100,999);
    }


    /**
     * 交通商家列表
     */
    public function trafficShops($where){
        $count      = $this->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=$this->where($where)->order('supplier_id desc')->limit($Page->firstRow.','.$Page->listRows)->select();
        $allAreas=S('allAreas');
        if(!$allAreas){
            $allAreas=M('areas')->where(['isShow'=>1,'areaFlag'=>1])->field('areaId,areaName')->select();
            $newArea=[];
            foreach ($allAreas as $k=>$v){
                $newArea[$v['areaId']]=$v['areaName'];
            }
            S('allAreas',$newArea,3600);
            $allAreas=$newArea;
        }

        foreach ($list as $k=>$v){

            $list[$k]['addr']=$allAreas[$v['country']].' '.$allAreas[$v['province']].' '.$allAreas[$v['city']].' '.$allAreas[$v['area']];
        }

        return ['list'=>$list,'show'=>$show];
    }

    /**
     * 开通与禁用用户状态
     */
    public function trafficStatusById($data){
        return $this->where(['supplier_id'=>$data['id']])->setField(['status'=>$data['status']]);
    }

    /**
     * 导出Excel
     */
    public  function exportExcelGroupList($where){
        $list=$this->where($where)->field('supplier_sn,supplier_name,type,Linkman,Linkman_phone,status')->select();
        foreach ($list as $k=>$v){
            $list[$k]['type']=self::suppierType($v['type']);
            $list[$k]['status']=$v['status']==1?'开通':'锁定';
        }
        $expCellName  = array(
            array('supplier_sn','编号'),
            array('supplier_name','商家名称'),
            array('type','项目'),
            array('Linkman','联系人'),
            array('Linkman_phone','联系电话'),
            array('status','状态'),
        );

        $fileName='交通供应商列表';
        switch ($where['type']){
            case 2:$fileName='酒店供应商';break;
            case 3:$fileName='保险供应商';break;
            case 4:$fileName='景区供应商';break;
        }
        parent::exportExcel($fileName,$expCellName,$list);
    }


    /**
     * 返回是哪一类型的商家
     */
    private function  suppierType($type){
        $name='';
        switch ($type){
            case 1:$name='交通供应商';break;
            case 2:$name='酒店供应商';break;
            case 3:$name='保险供应商';break;
            case 4:$name='景区供应商';break;
        }
        return $name;
    }


    /**
     * 添加线路
     */
    public function addTraffic($data){
        $newData=[];
        $room=$data['room'];
        $db=M('public_single_traffic_price');

        //判断是否重复添加
        $existsWhere['supplier_id']=$data['supplier_id'];
        $existsWhere['start_area']=$data['start_area'];
        $existsWhere['goal_area']=$data['goal_area'];
        $existsWhere['supplier_id']=$data['supplier_id'];

        foreach ($room as $k=>$v){
            $one=$v['child_foreign_cost']&&$v['adult_foreign_cost']&&$v['exchange_rate']&&$v['room_count'];
            $two=$v['child_actual_cost']&&$v['adult_actual_cost']&&$v['room_count'];
            $existsWhere['seat_id']=$v['room_id'];
            $exists=$db->where($existsWhere)->find();
            if($exists){
                continue;
            }
            if($one||$two){
                $v['is_review']=-1;
                $v['is_shelves']=-1;
                $v['total_foreign_cost']=$v['child_foreign_cost']+$v['adult_foreign_cost'];
                $v['total_actual_cost']=$v['child_actual_cost']+$v['adult_actual_cost'];
                $v['entry_id']=session('dealer_user.supplier_id');
                $v['supplier_id']=$data['supplier_id'];

                $v['traffic_id']=$data['traffic_id'];
                $v['start_country']=$data['start_country'];
                $v['start_province']=$data['start_province'];
                $v['start_city']=$data['start_city'];
                $v['start_area']=$data['start_area'];
                $v['goal_country']=$data['goal_country'];
                $v['goal_province']=$data['goal_province'];
                $v['goal_city']=$data['goal_city'];
                $v['goal_area']=$data['goal_area'];

                $v['create_time']=time();
                $v['product_sn']=self::LineSn();
                $v['seat_num']=$v['room_count'];
                unset($v['room_count']);
                unset($v['room_id']);
                $newData[]=$v;
            }
        }

        if(!$newData){
            return ['status'=>0,'msg'=>'数据填写不完整或者房间已添加过!'];
        }
        $res=  $db->addAll($newData);
        if($res){
            return  ['status'=>1,'msg'=>'添加成功!'];
        }
        return  ['status'=>0,'msg'=>'添加失败!'];

    }



    /**
     * 交通线路编号
     */
    public  function LineSn(){
        $product_sn= M('public_single_traffic_price')->order('product_sn desc')->getField('product_sn');
        if(!$product_sn){
            return "10000".mt_rand(100,999);
        }
        return $product_sn+mt_rand(100,999);
    }














    /**
     * 房间类型
     */
    public  function  hotelRoom(){
        return M('public_single_room')->select();
    }


    /**
     * 添加房间
     */
    public function addHotelRoom($data){
       /* $newData=[];
        $room=$data['room'];
        $db=M('public_single_room_price');
        foreach ($room as $k=>$v){
            $one=$v['child_foreign_cost']&&$v['adult_foreign_cost']&&$v['exchange_rate']&&$v['room_count'];
            $two=$v['child_actual_cost']&&$v['adult_actual_cost']&&$v['room_count'];
            $exists=$db->where(['supplier_id'=>$data['supplier_id'],'room_id'=>$v['room_id']])->find();
            if($exists){
                continue;
            }
            if($one||$two){
                $v['is_review']=-1;
                $v['is_shelves']=-1;
                $v['total_foreign_cost']=$v['child_foreign_cost']+$v['adult_foreign_cost'];
                $v['total_actual_cost']=$v['child_actual_cost']+$v['adult_actual_cost'];
                $v['entry_id']=session('dealer_user.supplier_id');
                $v['star_level']=$data['stars'];
                $v['supplier_id']=$data['supplier_id'];
                $v['create_time']=time();
                $v['product_sn']=self::roomSn();
                $newData[]=$v;
            }
        }

        if(!$newData){
            return ['status'=>0,'msg'=>'数据填写不完整或者房间已添加过!'];
        }
        $res=  $db->addAll($newData);
        if($res){
            return  ['status'=>1,'msg'=>'添加成功!'];
        }
        return  ['status'=>0,'msg'=>'添加失败!'];*/


        if(!$data['supplier_id']){
            return ['status'=>0,'msg'=>'添加失败!'];
        }

        $scenicData['room_name']=$data['room_name'];
        $scenicData['supplier_id']=$data['supplier_id'];
        $scenicData['star_level']=$data['star_level'];
        $scenicData['create_time']=time();
        M()->startTrans();
        $scenicRes=M('public_single_room')->add($scenicData);

        $price=$data['room']['id'];
        $priceData['room_id']=$scenicRes;
        $priceData['money_type']=$price['money_type'];
        $priceData['total_foreign_cost']=$price['adult_foreign_cost']+$price['child_foreign_cost'];
        $priceData['adult_foreign_cost']=$price['adult_foreign_cost'];
        $priceData['child_foreign_cost']=$price['child_foreign_cost'];
        $priceData['exchange_rate']=$price['exchange_rate'];

        $priceData['total_actual_cost']=$price['child_actual_cost']+$price['adult_actual_cost'];
        $priceData['child_actual_cost']=$price['child_actual_cost'];
        $priceData['adult_actual_cost']=$price['adult_actual_cost'];
        $priceData['room_count']=$price['room_count'];
        $priceData['star_level']=$data['star_level'];

        $priceData['create_time']=time();
        $priceData['entry_id']=session('dealer_user.supplier_id');
        $priceData['supplier_id']=$data['supplier_id'];
        $priceData['product_sn']=self::roomSn();

        $priceRes=M('public_single_room_price')->add($priceData);

        if($scenicRes&&$priceRes){
            M()->commit();
            return ['status'=>1,'msg'=>'添加成功!'];
        }
        M()->rollback();
        return ['status'=>0,'msg'=>'添加失败!'];



    }





    /**
     * 房间编号
     */
    public  function roomSn(){
        $product_sn= M('public_single_room_price')->order('product_sn desc')->getField('product_sn');
        if(!$product_sn){
            return "10000".mt_rand(100,999);
        }
        return $product_sn+mt_rand(100,999);
    }

    /**
     * 根据酒店单品供应商ID获取产品列表
     */
    public  function  hotelProductById($where){
        $db_prefix=C('DB_PREFIX');
        $count      = M('public_single_room_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('public_single_room_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_room as r on p.room_id=r.room_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,r.room_name')
            ->order('p.room_priceId desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }



    /**
     * 某个酒店供应商商品导出Excel
     */
    public  function hotelExportExcelBySupplierId($where){
        $db_prefix=C('DB_PREFIX');
        $list=M('public_single_room_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_room as r on p.room_id=r.room_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,r.room_name')
            ->order('p.room_priceId desc')
            ->select();
        foreach ($list as $k=>$v){
           $list[$k]['foreign_cost']="成人:".$v['adult_foreign_cost']."元/人 \n\r"."儿童:".$v['child_foreign_cost']."元/人";
           $list[$k]['actual_cost']="成人:".$v['adult_actual_cost']."元/人 \n\r"."儿童:".$v['child_actual_cost']."元/人";
           $list[$k]['is_review']=self::is_review($v['is_review']);
           $list[$k]['is_shelves']=$v['is_shelves']==1?'上架':'下架';
        }
        $expCellName  = array(
            array('si_name','供应商名称'),
            array('star_level','星级'),
            array('product_sn','编号'),
            array('room_name','房间'),
            array('foreign_cost','外币本价'),
            array('exchange_rate','汇率'),
            array('actual_cost','人民币成本价'),
            array('sp_name','操作人'),
            array('is_review','审核状态'),
            array('is_shelves','是否上架'),
        );


        $fileName=$list[0]['si_name']."产品列表";
        parent::exportExcel($fileName,$expCellName,$list);
    }

    /**
     * 某个酒店供应商商品导出Excel
     */
    public  function trafficExportExcelBySupplierId($where){
        $db_prefix=C('DB_PREFIX');
        $list=M('public_single_traffic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_traffic as r on p.traffic_id=r.traffic_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,r.traffic_name')
            ->order('p.traffic_priceId desc')
            ->select();
        foreach ($list as $k=>$v){
           $list[$k]['foreign_cost']="成人:".$v['adult_foreign_cost']."元/人 \n\r"."儿童:".$v['child_foreign_cost']."元/人";
           $list[$k]['actual_cost']="成人:".$v['adult_actual_cost']."元/人 \n\r"."儿童:".$v['child_actual_cost']."元/人";
           $list[$k]['is_review']=self::is_review($v['is_review']);
           $list[$k]['is_shelves']=$v['is_shelves']==1?'上架':'下架';
        }
        $expCellName  = array(
            array('si_name','供应商名称'),
            array('routes_name','线路名称'),
            array('product_sn','编号'),
            array('traffic_name','交通方式'),
            array('foreign_cost','外币本价'),
            array('exchange_rate','汇率'),
            array('actual_cost','人民币成本价'),
            array('sp_name','操作人'),
            array('is_review','审核状态'),
            array('is_shelves','是否上架'),
        );

        $fileName=$list[0]['si_name']."线路列表";
        parent::exportExcel($fileName,$expCellName,$list);
    }

    /**
     * 审核状态
     */
    private function is_review($status){
        $name='';
        switch ($status){
            case 1:$name='已审核';break;
            case -1:$name='未审核';break;
            case -2:$name='已拒绝';break;
        }
        return $name;
    }


    /**
     * 酒店单品上下架
     */
    public  function  hotelRoomShelvesById($data){
        $roomInfo=M('public_single_room_price')->where(array('room_priceId'=>$data['room_priceId']))->find();
        if($roomInfo['is_review']!=1){
            return ['status'=>-1,'msg'=>'操作失败,此房间尚未审核!'];
        }
        $res=M('public_single_room_price')->where(array('room_priceId'=>$data['room_priceId']))->setField(['is_shelves'=>$data['is_shelves']]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }

    /**
     * 线路单品上下架
     */
    public  function  trafficShelvesById($data){
        $roomInfo=M('public_single_traffic_price')->where(array('traffic_priceId'=>$data['traffic_priceId']))->find();
        if($roomInfo['is_review']!=1){
            return ['status'=>-1,'msg'=>'操作失败,此线路尚未审核!'];
        }
        $res=M('public_single_traffic_price')->where(array('traffic_priceId'=>$data['traffic_priceId']))->setField(['is_shelves'=>$data['is_shelves']]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }









    /**
     * 酒店单品审核
     */
    public  function  editHotelRoom($data){

        if(!$data['supplier_id']||!$data['room_priceId']||!$data['room_id']){
            return ['status'=>0,'msg'=>'操作失败!'];
        }

        $roomSave['star_level']=$data['star_level'];
        $roomSave['room_name']=$data['room_name'];
        $roomRes=M('public_single_room')->where(array('room_id'=>$data['room_id']))->save($roomSave);

        $save['audit_id']=session('dealer_user.supplier_id');

        foreach ($data['room'] as $k=>$v){
            $save['money_type']=$v['money_type'];
            $save['total_foreign_cost']=$v['child_foreign_cost']+$v['adult_foreign_cost'];
            $save['child_foreign_cost']=$v['child_foreign_cost'];
            $save['adult_foreign_cost']=$v['adult_foreign_cost'];
            $save['exchange_rate']=$v['exchange_rate'];

            $save['total_actual_cost']=$v['child_actual_cost']+$v['adult_actual_cost'];
            $save['child_actual_cost']=$v['child_actual_cost'];
            $save['adult_actual_cost']=$v['adult_actual_cost'];
            $save['room_count']=$v['room_count'];
            $save['is_review']=$data['is_review'];
        }

        $res=M('public_single_room_price')->where(array('room_priceId'=>$data['room_priceId']))->save($save);
        if($roomRes!==false&&$res!==false){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }

    /**
     * 获取酒店详情
     */
    public  function  getHotelDetail($room_priceId){
        return M('public_single_room_price as p')
            ->join('__PUBLIC_SINGLE_ROOM__ as r on p.room_id=r.room_id')
            ->where(array('room_priceId'=>$room_priceId))
            ->find();
    }

    /**
     * 交通方式列表,如汽车,轮船
     */
    public  function  trafficList(){
        return M('public_single_traffic')->select();
    }

    /**
     * 根据交通单品供应商ID获取产品列表
     */
    public  function  trafficProductById($where){
        $db_prefix=C('DB_PREFIX');
        $count      = M('public_single_traffic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('public_single_traffic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_traffic as r on p.traffic_id=r.traffic_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,r.traffic_name')
            ->order('p.traffic_id desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }

    /**
     * 获取线路详情
     */
    public  function  getTraficInfo($traffic_priceId){
        return M('public_single_traffic_price as p')->join('__PUBLIC_SINGLE_TRAFFIC__ as t on t.traffic_id=p.traffic_id')->where(['traffic_priceId'=>$traffic_priceId])->find();
    }

    /**
     * 查询出发城市及目的城市,用于编辑
     */
    public  function getTrafficCity($info){
        $list['start_province_list']=D('Platform')->getProvinceByCountryId($info['start_country']);
        $list['start_city_list']=D('Platform')->getCity($info['start_province']);
        $list['start_area_list']=D('Platform')->getArea($info['start_city']);

        $list['goal_province_list']=D('Platform')->getProvinceByCountryId($info['goal_country']);
        $list['goal_city_list']=D('Platform')->getCity($info['goal_province']);
        $list['goal_area_list']=D('Platform')->getArea($info['goal_city']);

        return $list;
    }





    /**
     * 编辑线路
     */
    public  function  editTraffic($data){
        $room=$data['room'];
        $save['is_review']=$data['is_review'];
        $save['audit_id']=session('dealer_user.supplier_id');
        foreach ($room as $k=>$v){
            $save['money_type']=$v['money_type'];
            $save['routes_name']=$v['routes_name'];
            $save['child_foreign_cost']=$v['child_foreign_cost'];
            $save['adult_foreign_cost']=$v['adult_foreign_cost'];
            $save['total_foreign_cost']=$v['child_foreign_cost']+$v['adult_foreign_cost'];
            $save['total_actual_cost']=$v['child_actual_cost']+$v['adult_actual_cost'];
            $save['exchange_rate']=$v['exchange_rate'];
            $save['child_actual_cost']=$v['child_actual_cost'];
            $save['adult_actual_cost']=$v['adult_actual_cost'];
            $save['seat_num']=$v['room_count'];
        }
        $res=M('public_single_traffic_price')->where(array('traffic_priceId'=>$data['traffic_priceId']))->save($save);
        if($res){
            return  ['status'=>1,'msg'=>'操作成功!'];
        }
        return  ['status'=>0,'msg'=>'操作失败!'];
    }


    /**
     * 添加景点及价格
     */
    public function  addScenic($data){

        if(!$data['supplier_id']){
            return ['status'=>0,'msg'=>'添加失败!'];
        }


        $scenicData['scenic_name']=$data['scenic_name'];
        $scenicData['supplier_id']=$data['supplier_id'];
        $scenicData['scenic_level']=$data['scenic_level'];
      /*  $scenicData['country']=$data['start_country'];
        $scenicData['province']=$data['start_province'];
        $scenicData['city']=$data['start_city'];
        $scenicData['area']=$data['start_area'];*/
        $scenicData['create_time']=time();
        M()->startTrans();
        $scenicRes=M('public_single_scenic')->add($scenicData);

        $price=$data['room']['id'];
        $priceData['scenic_id']=$scenicRes;
        $priceData['money_type']=$price['money_type'];
        $priceData['total_foreign_cost']=$price['adult_foreign_cost']+$price['child_foreign_cost'];
        $priceData['adult_foreign_cost']=$price['adult_foreign_cost'];
        $priceData['child_foreign_cost']=$price['child_foreign_cost'];
        $priceData['exchange_rate']=$price['exchange_rate'];

        $priceData['total_actual_cost']=$price['child_actual_cost']+$price['adult_actual_cost'];
        $priceData['child_actual_cost']=$price['child_actual_cost'];
        $priceData['adult_actual_cost']=$price['adult_actual_cost'];
        $priceData['seat_num']=$price['room_count'];
        $priceData['create_time']=time();
        $priceData['entry_id']=session('dealer_user.supplier_id');
        $priceData['supplier_id']=$data['supplier_id'];
        $priceData['product_sn']=self::scenicSn();

        $priceRes=M('public_single_scenic_price')->add($priceData);

        if($scenicRes&&$priceRes){
            M()->commit();
            return ['status'=>1,'msg'=>'添加成功!'];
        }
        M()->rollback();
        return ['status'=>0,'msg'=>'添加失败!'];

    }

    /**
     * 景区编号
     */
    public  function scenicSn(){
        $product_sn= M('public_single_scenic_price')->order('product_sn desc')->getField('product_sn');
        if(!$product_sn){
            return "10000".mt_rand(100,999);
        }
        return $product_sn+mt_rand(100,999);
    }


    /**
     * 根据景区单品供应商ID获取产品列表
     */
    public  function  scenicProductById($where){
        $db_prefix=C('DB_PREFIX');
        $count      = M('public_single_scenic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_scenic as sc on p.scenic_id=sc.scenic_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")

            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('public_single_scenic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_scenic as sc on sc.scenic_id=p.scenic_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,sc.*')
            ->order('p.scenic_priceId desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }


    /**
     * 某个景区供应商商品导出Excel
     */
    public  function scenicExportExcelBySupplierId($where){
        $db_prefix=C('DB_PREFIX');
        $list=M('public_single_scenic_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_scenic as sc on sc.scenic_id=p.scenic_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,sc.*')
            ->order('p.scenic_priceId desc')
            ->select();
        foreach ($list as $k=>$v){
            $list[$k]['foreign_cost']="成人:".$v['adult_foreign_cost']."元/人 \n\r"."儿童:".$v['child_foreign_cost']."元/人";
            $list[$k]['actual_cost']="成人:".$v['adult_actual_cost']."元/人 \n\r"."儿童:".$v['child_actual_cost']."元/人";
            $list[$k]['is_review']=self::is_review($v['is_review']);
            $list[$k]['is_shelves']=$v['is_shelves']==1?'上架':'下架';
        }
        $expCellName  = array(
            array('scenic_name','景区名称'),
            array('scenic_level','等级'),
            array('product_sn','编号'),
            array('foreign_cost','外币本价'),
            array('exchange_rate','汇率'),
            array('actual_cost','人民币成本价'),
            array('sp_name','操作人'),
            array('is_review','审核状态'),
            array('is_shelves','是否上架'),
        );
        $fileName=$list[0]['si_name']."景区列表";
        parent::exportExcel($fileName,$expCellName,$list);
    }



    /**
     * 景区单品上下架
     */
    public  function  scenicShelvesById($data){
        $roomInfo=M('public_single_scenic_price')->where(array('scenic_priceId'=>$data['scenic_priceId']))->find();
        if($roomInfo['is_review']!=1){
            return ['status'=>-1,'msg'=>'操作失败,此景区尚未审核!'];
        }
        $res=M('public_single_scenic_price')->where(array('scenic_priceId'=>$data['scenic_priceId']))->setField(['is_shelves'=>$data['is_shelves']]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }

    /**
     * 获取景区详情
     */
    public  function  getScenicInfo($scenic_priceId){
        $info=M('public_single_scenic_price as p')
            ->join('__PUBLIC_SINGLE_SCENIC__ as sc on sc.scenic_id=p.scenic_id')
            ->where(array('p.scenic_priceId'=>$scenic_priceId))
            ->find();
        return $info;
    }


    /**
     * 查询出景区所在地信息
     */
    public  function getScenicCity($info){
        $list['province_list']=D('Platform')->getProvinceByCountryId($info['country']);
        $list['city_list']=D('Platform')->getCity($info['province']);
        $list['area_list']=D('Platform')->getArea($info['city']);
        return $list;
    }


    /**
     * 审核景点
     */
    public  function  editScenic($data){
        if(!$data['supplier_id']||!$data['scenic_priceId']){
            return ['status'=>0,'msg'=>'操作失败!'];
        }


        $scenicData['scenic_name']=$data['scenic_name'];
        $scenicData['supplier_id']=$data['supplier_id'];
        /*$scenicData['scenic_level']=$data['scenic_level'];
        $scenicData['country']=$data['start_country'];
        $scenicData['province']=$data['start_province'];
        $scenicData['city']=$data['start_city'];
        $scenicData['area']=$data['start_area'];*/

        $scenicRes=M('public_single_scenic')->where(array('scenic_id'=>$data['scenic_id']))->save($scenicData);

        $price=$data['room']['id'];

        $priceData['money_type']=$price['money_type'];
        $priceData['total_foreign_cost']=$price['adult_foreign_cost']+$price['child_foreign_cost'];
        $priceData['adult_foreign_cost']=$price['adult_foreign_cost'];
        $priceData['child_foreign_cost']=$price['child_foreign_cost'];
        $priceData['exchange_rate']=$price['exchange_rate'];

        $priceData['total_actual_cost']=$price['child_actual_cost']+$price['adult_actual_cost'];
        $priceData['child_actual_cost']=$price['child_actual_cost'];
        $priceData['adult_actual_cost']=$price['adult_actual_cost'];
        $priceData['seat_num']=$price['room_count'];
        $priceData['is_review']=$data['is_review'];
        $priceData['audit_id']=session('dealer_user.supplier_id');

        $priceRes=M('public_single_scenic_price')->where(array('scenic_priceId'=>$data['scenic_priceId']))->save($priceData);

        if($scenicRes!==false&&$priceRes!==false){
            return ['status'=>1,'msg'=>'操作成功!'];
        }

        return ['status'=>0,'msg'=>'操作失败!'];
    }


    /**
     * 添加保险
     */
    public function addInsure($data){

        if(!$data['supplier_id']){
            return ['status'=>0,'msg'=>'添加失败!'];
        }
        $scenicData['insure_name']=$data['insure_name'];
        $scenicData['supplier_id']=$data['supplier_id'];
        /*  $scenicData['country']=$data['start_country'];
          $scenicData['province']=$data['start_province'];
          $scenicData['city']=$data['start_city'];
          $scenicData['area']=$data['start_area'];*/
        $scenicData['create_time']=time();
        M()->startTrans();
        $scenicRes=M('public_single_insure')->add($scenicData);

        $price=$data['room']['id'];
        $priceData['insure_id']=$scenicRes;
        $priceData['money_type']=$price['money_type'];
        $priceData['total_foreign_cost']=$price['adult_foreign_cost']+$price['child_foreign_cost'];
        $priceData['adult_foreign_cost']=$price['adult_foreign_cost'];
        $priceData['child_foreign_cost']=$price['child_foreign_cost'];
        $priceData['exchange_rate']=$price['exchange_rate'];

        $priceData['total_actual_cost']=$price['child_actual_cost']+$price['adult_actual_cost'];
        $priceData['child_actual_cost']=$price['child_actual_cost'];
        $priceData['adult_actual_cost']=$price['adult_actual_cost'];
        $priceData['seat_num']=$price['room_count'];
        $priceData['create_time']=time();
        $priceData['entry_id']=session('dealer_user.supplier_id');
        $priceData['supplier_id']=$data['supplier_id'];
        $priceData['product_sn']=self::insureSn();

        $priceRes=M('public_single_insure_price')->add($priceData);

        if($scenicRes&&$priceRes){
            M()->commit();
            return ['status'=>1,'msg'=>'添加成功!'];
        }
        M()->rollback();
        return ['status'=>0,'msg'=>'添加失败!'];
    }


    /**
     * 保险编号
     */
    public  function insureSn(){
        $product_sn= M('public_single_insure_price')->order('product_sn desc')->getField('product_sn');
        if(!$product_sn){
            return "10000".mt_rand(100,999);
        }
        return $product_sn+mt_rand(100,999);
    }



    /**
     * 根据景区单品供应商ID获取产品列表
     */
    public  function  insureProductById($where){
        $db_prefix=C('DB_PREFIX');
        $count      = M('public_single_insure_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_insure as sc on p.insure_id=sc.insure_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")

            ->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出
        // 进行分页数据查询 注意limit方法的参数要使用Page类的属性
        $list=M('public_single_insure_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_insure as sc on sc.insure_id=p.insure_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,sc.*')
            ->order('p.insure_priceId desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        return ['list'=>$list,'show'=>$show];

    }



    /**
     *  保险单品上下架
     */
    public  function  insureShelvesById($data){
        $roomInfo=M('public_single_insure_price')->where(array('insure_priceId'=>$data['insure_priceId']))->find();
        if($roomInfo['is_review']!=1){
            return ['status'=>-1,'msg'=>'操作失败,此保险尚未审核!'];
        }
        $res=M('public_single_insure_price')->where(array('insure_priceId'=>$data['insure_priceId']))->setField(['is_shelves'=>$data['is_shelves']]);
        if($res){
            return ['status'=>1,'msg'=>'操作成功!'];
        }
        return ['status'=>0,'msg'=>'操作失败!'];
    }


    /**
     * 审核保险
     */
    public function  editInsure($data){
        if(!$data['supplier_id']||!$data['insure_priceId']||!$data['insure_id']){
            return ['status'=>0,'msg'=>'添加失败!'];
        }
        $scenicData['insure_name']=$data['insure_name'];
        $scenicRes=M('public_single_insure')->where(['insure_id'=>$data['insure_id']])->save($scenicData);

        $price=$data['room']['id'];

        $priceData['money_type']=$price['money_type'];
        $priceData['total_foreign_cost']=$price['adult_foreign_cost']+$price['child_foreign_cost'];
        $priceData['adult_foreign_cost']=$price['adult_foreign_cost'];
        $priceData['child_foreign_cost']=$price['child_foreign_cost'];
        $priceData['exchange_rate']=$price['exchange_rate'];

        $priceData['total_actual_cost']=$price['child_actual_cost']+$price['adult_actual_cost'];
        $priceData['child_actual_cost']=$price['child_actual_cost'];
        $priceData['adult_actual_cost']=$price['adult_actual_cost'];
        $priceData['seat_num']=$price['room_count'];
        $priceData['is_review']=$data['is_review'];
        $priceData['audit_id']=session('dealer_user.supplier_id');

        $priceRes=M('public_single_insure_price')->where(['insure_priceId'=>$data['insure_priceId']])->save($priceData);
        if($scenicRes!==false&&$priceRes!==false){
            return ['status'=>1,'msg'=>'操作成功!'];
        }

        return ['status'=>0,'msg'=>'添加失败!'];
    }

    /**
     * 获取保险详情
     */
    public  function  getInsureDetail($insure_priceId){
        return M('public_single_insure_price as p')
            ->join('__PUBLIC_SINGLE_INSURE__ as r on p.insure_id=r.insure_id')
            ->where(array('insure_priceId'=>$insure_priceId))
            ->find();
    }



    /**
     * 某个保险供应商商品导出Excel
     */
    public  function insureExportExcelBySupplierId($where){
        $db_prefix=C('DB_PREFIX');
        $list=M('public_single_insure_price as p')
            ->join("{$db_prefix}public_single_supplier as si on p.supplier_id=si.supplier_id")
            ->join("{$db_prefix}public_single_insure as sc on sc.insure_id=p.insure_id")
            ->join("LEFT JOIN {$db_prefix}operator_line_supplier as sp on p.entry_id=sp.supplier_id")
            ->where($where)
            ->field('p.*,si.supplier_name as si_name,sp.supplier_name as sp_name,sc.*')
            ->order('p.insure_priceId desc')
            ->select();
        foreach ($list as $k=>$v){
            $list[$k]['foreign_cost']="成人:".$v['adult_foreign_cost']."元/人 \n\r"."儿童:".$v['child_foreign_cost']."元/人";
            $list[$k]['actual_cost']="成人:".$v['adult_actual_cost']."元/人 \n\r"."儿童:".$v['child_actual_cost']."元/人";
            $list[$k]['is_review']=self::is_review($v['is_review']);
            $list[$k]['is_shelves']=$v['is_shelves']==1?'上架':'下架';
        }
        $expCellName  = array(
            array('si_name','保险名称'),
            array('insure_name','保险名称'),
            array('product_sn','编号'),
            array('foreign_cost','外币本价'),
            array('exchange_rate','汇率'),
            array('actual_cost','人民币成本价'),
            array('sp_name','操作人'),
            array('is_review','审核状态'),
            array('is_shelves','是否上架'),
        );

        $fileName=$list[0]['si_name']."线路列表";
        parent::exportExcel($fileName,$expCellName,$list);
    }


    /**
     * 交通供应商合作记录
     */
    public  function  trafficPartnerRecord($id){

        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');

        //出团或回团使用了些单品供应商的团号列表
        $countWhere['o.order_status']=2;
        $countWhere['_string']="o.go_traffic_single_supplier_id={$id} or o.back_traffic_single_supplier_id=$id";
        $count=M('line_orders as o')
            ->where($countWhere)
            ->group('group_id')
            ->count();

        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出

        //包含了此单品供应的团号列表信息
        $groupList=M('line_orders as o')
            ->field('o.group_id,o.line_id,l.line_name,op.operator_name,g.closing_time,g.group_num,a.areaName')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as g on g.group_id=o.group_id")
            ->join('LEFT JOIN  __PUBLIC_LINE__ as l on l.line_id=o.line_id')
            ->join('LEFT JOIN  __AREAS__ as a on a.areaId=l.origin_id')
            ->join("LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin2_id")
            ->where($countWhere)
            ->group('o.group_id')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();



        //总人数
        $headcount=0;
        //总金额
        $totalMoney=0;

        $field="o.adult_num,o.child_num,o.oldMan_num,o.update_server_fee,in.adult_actual_cost,in.child_actual_cost,o.go_traffic_priceId,o.back_traffic_priceId";
        foreach ($groupList as $k=>$v){

            //出发交通价格统计
            $where['o.go_traffic_single_supplier_id']=$id;
            $where['o.group_id']=$v['group_id'];
            $where['o.order_status']=2;
            $list=M('line_orders as o')
                ->where($where)
                ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_PRICE__ as in on in.traffic_priceId=o.go_traffic_priceId")
                ->fine($field)
                ->select();

            foreach ($list as $kk=>$vv){
                $headcount+=$v['adult_num'];
                $headcount+=$v['child_num'];
                $headcount+=$v['oldMan_num'];

                $totalMoney+=$v['adult_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['oldMan_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['child_num']*$v['child_actual_cost'];

                //判断是否有升级服务费用
                $update_server=json_decode($v['update_server_fee'],true);
                $trafficInfo=$update_server['go_traffic'];
                if($trafficInfo){
                    foreach ($trafficInfo as $tk=>$tv){
                        if($tk==$v['go_traffic_priceId']){
                            $totalMoney+=$tv['diff_price'];
                        }
                    }
                }

            }

            $list=[];
            //回程交通价格统计
            $where['o.back_traffic_single_supplier_id']=$id;
            $where['o.group_id']=$v['group_id'];
            $where['o.order_status']=2;
            $list=M('line_orders as o')
                ->where($where)
                ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_PRICE__ as in on in.traffic_priceId=o.back_traffic_priceId")
                ->fine($field)
                ->select();

            foreach ($list as $kk=>$vv){
                $headcount+=$v['adult_num'];
                $headcount+=$v['child_num'];
                $headcount+=$v['oldMan_num'];

                $totalMoney+=$v['adult_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['oldMan_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['child_num']*$v['child_actual_cost'];

                //升级费用统计
                //判断是否有升级服务费用
                $update_server=json_decode($v['update_server_fee'],true);
                $trafficInfo=$update_server['back_traffic'];
                if($trafficInfo){
                    foreach ($trafficInfo as $tk=>$tv){
                        if($tk==$v['back_traffic_priceId']){
                            $totalMoney+=$tv['diff_price'];
                        }
                    }
                }
            }

            //总人数
            $groupList[$k]['headcount']=$headcount;
            //总结算金额
            $groupList[$k]['cost']=$totalMoney;
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }

        return ['list'=>$groupList,'show'=>$show];
    }



    /**
     * @param $id 交通供应商supplier_id
     * @return array 导出交通单品供应商的合作记录
     */
    public function exportTrafficPartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');

        //出团或回团使用了些单品供应商的团号列表
        $countWhere['o.order_status']=2;
        $countWhere['_string']="o.go_traffic_single_supplier_id={$id} or o.back_traffic_single_supplier_id=$id";


        //包含了此单品供应的团号列表信息
        $groupList=M('line_orders as o')
            ->field('o.group_id,o.line_id,l.line_name,op.operator_name,g.closing_time,g.group_num,a.areaName')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as g on g.group_id=o.group_id")
            ->join('LEFT JOIN  __PUBLIC_LINE__ as l on l.line_id=o.line_id')
            ->join('LEFT JOIN  __AREAS__ as a on a.areaId=l.origin_id')
            ->join("LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin2_id")
            ->where($countWhere)
            ->group('o.group_id')
            ->select();



        //总人数
        $headcount=0;
        //总金额
        $totalMoney=0;

        $field="o.adult_num,o.child_num,o.oldMan_num,o.update_server_fee,in.adult_actual_cost,in.child_actual_cost,o.go_traffic_priceId,o.back_traffic_priceId";
        foreach ($groupList as $k=>$v){

            $groupList[$k]['closing_time']=date('Y-m-d',$v['closing_time']);
            $groupList[$k]['item']='交通';
            //出发交通价格统计
            $where['o.go_traffic_single_supplier_id']=$id;
            $where['o.group_id']=$v['group_id'];
            $where['o.order_status']=2;
            $list=M('line_orders as o')
                ->where($where)
                ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_PRICE__ as in on in.traffic_priceId=o.go_traffic_priceId")
                ->fine($field)
                ->select();

            foreach ($list as $kk=>$vv){
                $headcount+=$v['adult_num'];
                $headcount+=$v['child_num'];
                $headcount+=$v['oldMan_num'];

                $totalMoney+=$v['adult_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['oldMan_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['child_num']*$v['child_actual_cost'];

                //判断是否有升级服务费用
                $update_server=json_decode($v['update_server_fee'],true);
                $trafficInfo=$update_server['go_traffic'];
                if($trafficInfo){
                    foreach ($trafficInfo as $tk=>$tv){
                        if($tk==$v['go_traffic_priceId']){
                            $totalMoney+=$tv['diff_price'];
                        }
                    }
                }

            }

            $list=[];
            //回程交通价格统计
            $where['o.back_traffic_single_supplier_id']=$id;
            $where['o.group_id']=$v['group_id'];
            $list=M('line_orders as o')
                ->where($where)
                ->join("LEFT JOIN __PUBLIC_SINGLE_TRAFFIC_PRICE__ as in on in.traffic_priceId=o.back_traffic_priceId")
                ->fine($field)
                ->select();

            foreach ($list as $kk=>$vv){
                $headcount+=$v['adult_num'];
                $headcount+=$v['child_num'];
                $headcount+=$v['oldMan_num'];

                $totalMoney+=$v['adult_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['oldMan_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['child_num']*$v['child_actual_cost'];

                //升级费用统计
                //判断是否有升级服务费用
                $update_server=json_decode($v['update_server_fee'],true);
                $trafficInfo=$update_server['back_traffic'];
                if($trafficInfo){
                    foreach ($trafficInfo as $tk=>$tv){
                        if($tk==$v['back_traffic_priceId']){
                            $totalMoney+=$tv['diff_price'];
                        }
                    }
                }
            }

            //总人数
            $groupList[$k]['headcount']=$headcount;
            //总结算金额
            $groupList[$k]['cost']=$totalMoney;
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }


        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('line_name','线路名称'),
            array('areaName','出发城市'),
            array('group_num','团号'),
            array('item','项目'),
            array('headcount','数量'),
            array('cost','结算金额'),
            array('closing_time','结算时间'),
            array('operator_name','操作人（结算）'),
        );

        $fileName=$list[0]['supplier_name']."交通供应商合作记录";
        parent::exportExcel($fileName,$expCellName,$groupList);

    }


    /**
     * 保险供应商合作记录
     */
    public  function  insurePartnerRecord($id){


        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');

        //包含了此单品供应的团号总数
        $countWhere['o.order_status']=2;
        $countWhere['o.insure_single_supplier_id']=$id;
        $count=M('line_orders as o')
            ->where($countWhere)
            ->group('group_id')
            ->count();
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出


        //包含了此单品供应的团号列表信息
        $groupList=M('line_orders as o')
            ->field('o.group_id,o.line_id,l.line_name,op.operator_name,g.closing_time,g.group_num,a.areaName')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as g on g.group_id=o.group_id")
            ->join('LEFT JOIN  __PUBLIC_LINE__ as l on l.line_id=o.line_id')
            ->join('LEFT JOIN  __AREAS__ as a on a.areaId=l.origin_id')
            ->join("LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin2_id")
            ->where($countWhere)
            ->group('o.group_id')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        $field="o.adult_num,o.child_num,o.oldMan_num,o.update_server_fee,in.adult_actual_cost,in.child_actual_cost";

        foreach ($groupList as $k=>$v){
            $where['o.insure_single_supplier_id']=$id;
            $where['o.group_id']=$v['group_id'];
            $list=M('line_orders as o')
                ->where($where)
                ->join("LEFT JOIN __PUBLIC_SINGLE_INSURE_PRICE__ as in on in.insure_priceId=o.insure_priceId")
                ->fine($field)
                ->select();

            //总人数
            $headcount=0;
            //总金额
            $totalMoney=0;
            foreach ($list as $kk=>$vv){
                $headcount+=$v['adult_num'];
                $headcount+=$v['child_num'];
                $headcount+=$v['oldMan_num'];

                $totalMoney+=$v['adult_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['oldMan_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['child_num']*$v['child_actual_cost'];

            }
            //总人数
            $groupList[$k]['headcount']=$headcount;
            //总结算金额
            $groupList[$k]['cost']=$totalMoney;
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }

        return ['list'=>$groupList,'show'=>$show];
    }


    /**
     * 保险单品供应商合作记录导出EXCEL
     */
    public  function exportInsurePartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');
        $countWhere['o.order_status']=2;
        $countWhere['o.insure_single_supplier_id']=$id;

        //包含了此单品供应的团号列表信息
        $groupList=M('line_orders as o')
            ->field('o.group_id,o.line_id,l.line_name,op.operator_name,g.closing_time,g.group_num,a.areaName')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as g on g.group_id=o.group_id")
            ->join('LEFT JOIN  __PUBLIC_LINE__ as l on l.line_id=o.line_id')
            ->join('LEFT JOIN  __AREAS__ as a on a.areaId=l.origin_id')
            ->join("LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin2_id")
            ->where($countWhere)
            ->group('o.group_id')
            ->select();

        $field="o.adult_num,o.child_num,o.oldMan_num,o.update_server_fee,in.adult_actual_cost,in.child_actual_cost";

        foreach ($groupList as $k=>$v){
            $groupList[$k]['closing_time']=date('Y-m-d',$v['closing_time']);
            $groupList[$k]['item']='保险';
            $where['o.insure_single_supplier_id']=$id;
            $where['o.group_id']=$v['group_id'];
            $where['o.order_status']=2;
            $list=M('line_orders as o')
                ->where($where)
                ->join("LEFT JOIN __PUBLIC_SINGLE_INSURE_PRICE__ as in on in.insure_priceId=o.insure_priceId")
                ->fine($field)
                ->select();

            //总人数
            $headcount=0;
            //总金额
            $totalMoney=0;
            foreach ($list as $kk=>$vv){
                $headcount+=$v['adult_num'];
                $headcount+=$v['child_num'];
                $headcount+=$v['oldMan_num'];

                $totalMoney+=$v['adult_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['oldMan_num']*$v['adult_actual_cost'];
                $totalMoney+=$v['child_num']*$v['child_actual_cost'];

            }
            //总人数
            $groupList[$k]['headcount']=$headcount;
            //总结算金额
            $groupList[$k]['cost']=$totalMoney;
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }


        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('line_name','线路名称'),
            //array('areaName','出发城市'),
            array('group_num','团号'),
            array('item','项目'),
            array('headcount','数量'),
            array('cost','结算金额'),
            array('closing_time','结算时间'),
            array('operator_name','操作人（结算）'),
        );

        $fileName=$list[0]['supplier_name']."保险供应商合作记录";
        parent::exportExcel($fileName,$expCellName,$groupList);


    }



    /**
     * 景区供应商
     */
    public function  scenicPartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');

        //包含了此单品供应的团号总数
        $countWhere['o.order_status']=2;
        $countWhere['_string']="FIND_IN_SET({$id},o.scenic_single_supplier_id)";
        $count=M('line_orders as o')
            ->where($countWhere)
            ->group('group_id')
            ->count();
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出

        //包含了此单品供应的团号列表信息
        $groupList=M('line_orders as o')
            ->field('o.group_id,o.line_id,l.line_name,op.operator_name,g.closing_time,g.group_num,a.areaName')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as g on g.group_id=o.group_id")
            ->join('LEFT JOIN  __PUBLIC_LINE__ as l on l.line_id=o.line_id')
            ->join('LEFT JOIN  __AREAS__ as a on a.areaId=l.origin_id')
            ->join("LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin2_id")
            ->where($countWhere)
            ->group('group_id')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        $field="o.adult_num,o.child_num,o.oldMan_num,o.update_server_fee,o.scenic_single_supplier_id,o.scenic_priceId";

        foreach ($groupList as $k=>$v){

            //订单人数列表及酒店ID
            $countWhere['_string']="FIND_IN_SET({$id},o.scenic_single_supplier_id)";
            $where['o.group_id']=$v['group_id'];
            $where['o.order_status']=2;
            $list=M('line_orders as o')
                ->where($where)
                ->fine($field)
                ->select();
            $roomId=[];

            //所有房间ID
            foreach ($list as $lk=>$lv){
                $temp=explode(',',$lv['scenic_priceId']);
                foreach ($temp as $tv){
                    $roomId[]=$tv;
                }
            }
            $roomId=array_unique($roomId);

            //此团所含有的所有酒店房间价格列表,且是$id商家的酒店房间
            $roomPriceList=M('public_single_scenic_price')->where(array('scenic_priceId'=>array('in',$roomId),'supplier_id'=>$id))->field('supplier_id,room_priceId,adult_actual_cost,child_actual_cost')->select();

            //总人数
            $headcount=0;
            //总金额
            $totalMoney=0;
            foreach ($list as $kk=>$vv){

                $orderRoomList=explode(',',$vv['scenic_priceId']);
                foreach ($orderRoomList as $ok=>$ov){
                    foreach ($roomPriceList as $rk=>$rv){
                        if($rv['scenic_priceId']==$ov){
                            //是$id酒店的房间才进行统计数据
                            $headcount+=$vv['adult_num'];
                            $headcount+=$vv['child_num'];
                            $headcount+=$vv['oldMan_num'];
                            $totalMoney+=$vv['adult_num']*$rv['adult_actual_cost'];
                            $totalMoney+=$vv['oldMan_num']*$rv['adult_actual_cost'];
                            $totalMoney+=$vv['child_num']*$rv['child_actual_cost'];
                        }
                    }
                }

            }
            //总人数
            $groupList[$k]['headcount']=$headcount;
            //总结算金额
            $groupList[$k]['cost']=$totalMoney;
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }
        return ['list'=>$groupList,'show'=>$show];
    }


    /**
     * 景区单品供应商导出 EXCEL
     */
    public function  exportScenicPartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');

        //包含了此单品供应的团号总数
        $countWhere['o.order_status']=2;
        $countWhere['_string']="FIND_IN_SET({$id},o.scenic_single_supplier_id)";


        //包含了此单品供应的团号列表信息
        $groupList=M('line_orders as o')
            ->field('o.group_id,o.line_id,l.line_name,op.operator_name,g.closing_time,g.group_num,a.areaName')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as g on g.group_id=o.group_id")
            ->join('LEFT JOIN  __PUBLIC_LINE__ as l on l.line_id=o.line_id')
            ->join('LEFT JOIN  __AREAS__ as a on a.areaId=l.origin_id')
            ->join("LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin2_id")
            ->where($countWhere)
            ->group('group_id')
            ->select();

        $field="o.adult_num,o.child_num,o.oldMan_num,o.update_server_fee,o.scenic_single_supplier_id,o.scenic_priceId";

        foreach ($groupList as $k=>$v){

            $groupList[$k]['closing_time']=date('Y-m-d',$v['closing_time']);
            $groupList[$k]['item']='景区';


            //订单人数列表及酒店ID
            $countWhere['_string']="FIND_IN_SET({$id},o.scenic_single_supplier_id)";
            $where['o.group_id']=$v['group_id'];
            $where['o.order_status']=2;
            $list=M('line_orders as o')
                ->where($where)
                ->fine($field)
                ->select();
            $roomId=[];

            //所有房间ID
            foreach ($list as $lk=>$lv){
                $temp=explode(',',$lv['scenic_priceId']);
                foreach ($temp as $tv){
                    $roomId[]=$tv;
                }
            }
            $roomId=array_unique($roomId);

            //此团所含有的所有酒店房间价格列表,且是$id商家的酒店房间
            $roomPriceList=M('public_single_scenic_price')->where(array('scenic_priceId'=>array('in',$roomId),'supplier_id'=>$id))->field('supplier_id,room_priceId,adult_actual_cost,child_actual_cost')->select();

            //总人数
            $headcount=0;
            //总金额
            $totalMoney=0;
            foreach ($list as $kk=>$vv){

                $orderRoomList=explode(',',$vv['scenic_priceId']);
                foreach ($orderRoomList as $ok=>$ov){
                    foreach ($roomPriceList as $rk=>$rv){
                        if($rv['scenic_priceId']==$ov){
                            //是$id酒店的房间才进行统计数据
                            $headcount+=$vv['adult_num'];
                            $headcount+=$vv['child_num'];
                            $headcount+=$vv['oldMan_num'];
                            $totalMoney+=$vv['adult_num']*$rv['adult_actual_cost'];
                            $totalMoney+=$vv['oldMan_num']*$rv['adult_actual_cost'];
                            $totalMoney+=$vv['child_num']*$rv['child_actual_cost'];
                        }
                    }
                }

            }
            //总人数
            $groupList[$k]['headcount']=$headcount;
            //总结算金额
            $groupList[$k]['cost']=$totalMoney;
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }

        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('line_name','线路名称'),
            //array('areaName','出发城市'),
            array('group_num','团号'),
            array('item','项目'),
            array('headcount','数量'),
            array('cost','结算金额'),
            array('closing_time','结算时间'),
            array('operator_name','操作人（结算）'),
        );

        $fileName=$list[0]['supplier_name']."景区供应商合作记录";
        parent::exportExcel($fileName,$expCellName,$groupList);

    }



    /**
     * 酒店供应商合作记录
     */
    public function  hotelPartnerRecord($id){
        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');

        //包含了此单品供应的团号总数
        $countWhere['o.order_status']=2;
        $countWhere['_string']="FIND_IN_SET({$id},o.room_single_supplier_id)";
        $count=M('line_orders as o')
            ->where($countWhere)
            ->group('group_id')
            ->count();
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数(25)
        $show       = $Page->show();// 分页显示输出

        //包含了此单品供应的团号列表信息
        $groupList=M('line_orders as o')
            ->field('o.group_id,o.line_id,l.line_name,op.operator_name,g.closing_time,g.group_num,a.areaName')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as g on g.group_id=o.group_id")
            ->join('LEFT JOIN  __PUBLIC_LINE__ as l on l.line_id=o.line_id')
            ->join('LEFT JOIN  __AREAS__ as a on a.areaId=l.origin_id')
            ->join("LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin2_id")
            ->where($countWhere)
            ->group('group_id')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();

        $field="o.adult_num,o.child_num,o.oldMan_num,o.update_server_fee,o.room_single_supplier_id,o.room_priceId";

        foreach ($groupList as $k=>$v){

            //订单人数列表及酒店ID
            $countWhere['_string']="FIND_IN_SET({$id},o.room_single_supplier_id)";
            $where['o.group_id']=$v['group_id'];
            $where['o.order_status']=2;
            $list=M('line_orders as o')
                ->where($where)
                ->fine($field)
                ->select();
            $roomId=[];

            //所有房间ID
            foreach ($list as $lk=>$lv){
                $temp=explode(',',$lv['room_priceId']);
                foreach ($temp as $tv){
                    $roomId[]=$tv;
                }
            }
            $roomId=array_unique($roomId);

            //此团所含有的所有酒店房间价格列表,且是$id商家的酒店房间
            $roomPriceList=M('public_single_room_price')->where(array('room_priceId'=>array('in',$roomId),'supplier_id'=>$id))->field('supplier_id,room_priceId,adult_actual_cost,child_actual_cost')->select();

            //总人数
            $headcount=0;
            //总金额
            $totalMoney=0;
            foreach ($list as $kk=>$vv){

                $orderRoomList=explode(',',$vv['room_priceId']);
                foreach ($orderRoomList as $ok=>$ov){
                    foreach ($roomPriceList as $rk=>$rv){
                        if($rv['room_priceId']==$ov){
                            //是$id酒店的房间才进行统计数据
                            $headcount+=$vv['adult_num'];
                            $headcount+=$vv['child_num'];
                            $headcount+=$vv['oldMan_num'];
                            $totalMoney+=$vv['adult_num']*$rv['adult_actual_cost'];
                            $totalMoney+=$vv['oldMan_num']*$rv['adult_actual_cost'];
                            $totalMoney+=$vv['child_num']*$rv['child_actual_cost'];
                            //判断是否有升级服务费用
                            $update_server=json_decode($vv['update_server_fee'],true);
                            $updateInfo=$update_server['hotel'];
                            if($updateInfo){
                                foreach ($updateInfo as $uk=>$uv){
                                    if($uk==$rv['room_priceId']){
                                        $totalMoney+=$tv['diff_price'];
                                    }
                                }
                            }

                        }
                    }
                }

            }
            //总人数
            $groupList[$k]['headcount']=$headcount;
            //总结算金额
            $groupList[$k]['cost']=$totalMoney;
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }
        return ['list'=>$groupList,'show'=>$show];
    }


    /**
     * 导出酒店单品供应商合作记录
     *
     */
    public  function  exportHotelPartnerRecord(){

        //供应商名称
        $supplier_name=M('public_single_supplier')->where(array('supplier_id'=>$id))->getField('supplier_name');

        //包含了此单品供应的团号总数
        $countWhere['o.order_status']=2;
        $countWhere['_string']="FIND_IN_SET({$id},o.room_single_supplier_id)";

        //包含了此单品供应的团号列表信息
        $groupList=M('line_orders as o')
            ->field('o.group_id,o.line_id,l.line_name,op.operator_name,g.closing_time,g.group_num,a.areaName')
            ->join("LEFT JOIN __PUBLIC_GROUP__ as g on g.group_id=o.group_id")
            ->join('LEFT JOIN  __PUBLIC_LINE__ as l on l.line_id=o.line_id')
            ->join('LEFT JOIN  __AREAS__ as a on a.areaId=l.origin_id')
            ->join("LEFT JOIN __OPERATOR__ as op on op.operator_id=g.admin2_id")
            ->where($countWhere)
            ->group('group_id')
            ->select();

        $field="o.adult_num,o.child_num,o.oldMan_num,o.update_server_fee,o.room_single_supplier_id,o.room_priceId";

        foreach ($groupList as $k=>$v){

            $groupList[$k]['closing_time']=date('Y-m-d',$v['closing_time']);
            $groupList[$k]['item']='房间';

            //订单人数列表及酒店ID
            $countWhere['_string']="FIND_IN_SET({$id},o.room_single_supplier_id)";
            $where['o.group_id']=$v['group_id'];
            $where['o.order_status']=2;
            $list=M('line_orders as o')
                ->where($where)
                ->fine($field)
                ->select();
            $roomId=[];

            //所有房间ID
            foreach ($list as $lk=>$lv){
                $temp=explode(',',$lv['room_priceId']);
                foreach ($temp as $tv){
                    $roomId[]=$tv;
                }
            }
            $roomId=array_unique($roomId);

            //此团所含有的所有酒店房间价格列表,且是$id商家的酒店房间
            $roomPriceList=M('public_single_room_price')->where(array('room_priceId'=>array('in',$roomId),'supplier_id'=>$id))->field('supplier_id,room_priceId,adult_actual_cost,child_actual_cost')->select();

            //总人数
            $headcount=0;
            //总金额
            $totalMoney=0;
            foreach ($list as $kk=>$vv){

                $orderRoomList=explode(',',$vv['room_priceId']);
                foreach ($orderRoomList as $ok=>$ov){
                    foreach ($roomPriceList as $rk=>$rv){
                        if($rv['room_priceId']==$ov){
                            //是$id酒店的房间才进行统计数据
                            $headcount+=$vv['adult_num'];
                            $headcount+=$vv['child_num'];
                            $headcount+=$vv['oldMan_num'];
                            $totalMoney+=$vv['adult_num']*$rv['adult_actual_cost'];
                            $totalMoney+=$vv['oldMan_num']*$rv['adult_actual_cost'];
                            $totalMoney+=$vv['child_num']*$rv['child_actual_cost'];
                            //判断是否有升级服务费用
                            $update_server=json_decode($vv['update_server_fee'],true);
                            $updateInfo=$update_server['hotel'];
                            if($updateInfo){
                                foreach ($updateInfo as $uk=>$uv){
                                    if($uk==$rv['room_priceId']){
                                        $totalMoney+=$tv['diff_price'];
                                    }
                                }
                            }

                        }
                    }
                }

            }
            //总人数
            $groupList[$k]['headcount']=$headcount;
            //总结算金额
            $groupList[$k]['cost']=$totalMoney;
            //供应商名称
            $groupList[$k]['supplier_name']=$supplier_name;
        }


        $expCellName  = array(
            array('supplier_name','供应商名称'),
            array('line_name','线路名称'),
            //array('areaName','出发城市'),
            array('group_num','团号'),
            array('item','项目'),
            array('headcount','数量'),
            array('cost','结算金额'),
            array('closing_time','结算时间'),
            array('operator_name','操作人（结算）'),
        );

        $fileName=$list[0]['supplier_name']."酒店供应商合作记录";
        parent::exportExcel($fileName,$expCellName,$groupList);
    }

}

