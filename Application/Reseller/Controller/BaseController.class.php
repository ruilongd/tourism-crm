<?php
namespace Reseller\Controller;

use Think\Controller;

/**
 * Base基类控制器
 */
class BaseController extends Controller
{
    protected $admin_id;
    public $nav_data;
    /**
     * 初始化方法
     */
    public function __construct()
    {
        parent::__construct();
        $auth      = new \Think\Auth();
        $rule_name = MODULE_NAME . '/' . CONTROLLER_NAME . '/' . ACTION_NAME;
        //不需要验证的控制器方法列表
        $notValidate = C('NOT_VALIDATE');

        //登录验证
        if (!session('reseller_user')) {
            header('Location:' . U('reseller/Login/login'));
            exit();
        }
        //超级管理员ID
        $userInfo = session('reseller_user');
        if ($userInfo['pid'] == 0) {
            $this->admin_id = $userInfo['reseller_id'];
        } else {
            $this->admin_id = $userInfo['pid'];
        }

        //登录控制器不用验证
        if (!in_array($rule_name, $notValidate)) {
            $result = $auth->check($rule_name, session('reseller_user.reseller_id'));
            if (!$result) {
                $this->error('您没有权限访问', U('Reseller/Login/login'));
            }
        }
        // 分配菜单数据  'platform'=>2 在模型里面配置一下参数 1运营商,2经销商,3分销
        $nav_data = D('AdminNav')->getTreeData('level', 'order_number,id');
        $assign = array(
            'nav_data' => $nav_data,
        );
        $this->assign($assign);
    }

    /**
     * 用于数据展示
     * @param  [array] $data [description]
     * @param  [array] $exclude [不需展示的数据的键名]
     */
    public function dataToDisplay($data, $exclude = '')
    {
        if (is_array($exclude)) {
            foreach ($exclude as $v) {
                unset($data[$v]);
            }
        }

        foreach ($data as $k => $v) {
            $this->$k = $v;
        }

    }

    public function _empty(){
        echo '<h1>暂无权限</h1>';
    }
}
