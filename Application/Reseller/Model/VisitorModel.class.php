<?php
namespace Reseller\Model;
/**
 * 游客(会员)资料模型
 */
class VisitorModel extends BaseModel{

    protected $tableName = 'public_visitor_info';


    // 自动验证
    protected $_validate=array(
        array('visitor_name','require','会员名不能为空',1,'',3), // 验证字段必填
        array('card_num','require','会员卡号不能为空!',1,'',3), // 验证字段必填
        array('visitor_sex','require','请选择会员性别!',1,'',3), // 验证字段必填
        array('documents_num','require','会员证件号不能为空!',1,'',3), // 验证字段必填
        array('visitor_phone','checkPhone','手机号码格式错误!',1,'callback',3), // 验证字段必填
    );


    /**
     * 判断手机号是否正确
     */
    public  function  checkPhone(){
        $visitor_phone = I('visitor_phone');

        if(preg_match("/^1[34578]{1}\d{9}$/",$visitor_phone)){
           return true;
        }

        return false;

    }


    /**
     * 生成新的会员卡号
     */
    public  function cardNum(){
        $date = date('YmdHis',time());
        $card_num = $date.time().mt_rand(100000,999999);
        $isHave =$this->where(array('card_num'=>$card_num))->getField('card_num');
        if (!$isHave) {
        	return $card_num;
        }
        return $card_num + mt_rand(100,999);
    }

 	/**
     * 会员列表数据
     */
    public function getAllVisitorInfo($where){

        $count      = $this->where($where)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,15);
        $show       = $Page->show();// 分页显示输出
        $list=$this
            ->alias('i')
            ->field('i.*,r.reseller_name as sale_man')
            ->join('__OPERATOR_LINE_RESELLER__ as r on i.sales_id = r.reseller_id and i.reseller_id='.session('reseller_user.reseller_id'))
            ->where($where)
            ->order('visitor_id desc')
            ->limit($Page->firstRow.','.$Page->listRows)
            ->select();
        $this->arrangeVisitor($list);
        return ['list'=>$list,'show'=>$show];
    }

    /**
     * 会员数据整理
     */
    public function arrangeVisitor(&$info){
        foreach ($info as $k=>$v){
            //性别判断
            if($v['visitor_sex'] == 1) $info[$k]['visitor_sex'] = '男';
            elseif($v['visitor_sex'] == 2) $info[$k]['visitor_sex'] = '女';

            $info[$k]['entry_time'] = date('Y-m-d H:i', $v['entry_time']);
        }
    }

 	/**
     * 添加会员
     */
    public function addVisitor($data){
    	$admin_id=session('reseller_user.pid')==0?session('reseller_user.reseller_id'):session('reseller_user.pid');
        $data['reseller_id']=$admin_id;
        $data['entry_time'] = time();
        $data['sales_id'] = $admin_id;
        $data['entry_id'] = $admin_id;
        $data['visitor_status'] = 1;
        $data['source_type'] = 2;
        // unset($data['visitor_id']);
        if($this->create($data)){
            return $this->add($data);
        }

        return false;
    }
    /**
     * 修改会员信息
     */
    public function editVisitor($data){
    		// 对data数据进行验证
        if(!$data=$this->create($data)){
            // 验证不通过返回错误
            return false;
        }else{
            // 验证通过
        	$map['visitor_id']=$data['visitor_id'];
            unset($data['visitor_id']);
            $result=$this
                ->where(array($map))
                ->save($data);
            return $result;
        }

    }
    /**
     * 根据ID 获取会员资料
     */
    public function getVisitorInfoById($id){
    	$visitorInfo=$this->where(array('visitor_id'=>$id))->select();
    	return $visitorInfo[0];
    }

    /**
     * 开通与锁定会员状态
     */
    public function changeVisitorStatusById($data){
        return $this->where(['visitor_id'=>$data['uid']])->setField(['visitor_status'=>$data['status']]);
    }


    /**
     * Excel导出会员信息列表-根据页面当前的筛选条件筛选
     */
    public function outVisitorExportExcel(){
    	$where = session('VISITOR_WHERE');
    	$list=$this->where($where)->order('entry_time desc')->select();
    	foreach ($list as $k=>$v){
            $list[$k]['source_type']=$v['source_type']==1?'直营门店':'分销商';
            $list[$k]['visitor_status']=$v['visitor_status']==1?'开通':'锁定';
            $list[$k]['visitor_sex']=$v['status']==1?'男':'女';
            $list[$k]['entry_time']=date('Y-m-d',$v['entry_time']);
        }
        $expCellName  = array(
            array('visitor_name','姓名'),
            array('visitor_sex','性别'),
            array('documents_num','证件号'),
            array('visitor_phone','电话'),
            array('card_num','卡号'),
            array('source_type','来源属性'),
            array('total_consumption','消费总额'),
            array('total_grade','累计积分'),
            array('consume_num','消费次数'),
            array('entry_time','创建时间'),
        );

        $fileName='会员资料表';
        parent::exportExcel($fileName,$expCellName,$list);
    }

    /**
     * 获取会员等级列表
     */
    public function getGradeConfing(){
        $reseller_id = session('reseller_user.pid')==0?session('reseller_user.reseller_id'):session('reseller_user.pid');
        $gradeInfo = M('public_grade_configs')->where(array('reseller_id'=>$reseller_id))->select();
        return $gradeInfo;
    }

    /**
     * 删除单条会员等级设置
     */
    public function delGrade(){
        $id = I('id');
        if(!is_numeric($id)) return ['status'=> -1, '非法操作'];

        $re = M('public_grade_configs')
            ->where(['grade_id' => $id])
            ->delete();
        if($re !== false)
            return ['status' => 1, '成功删除'];
        return ['status' => -2, '删除失败'];
    }

    /**
     *  批量保存会员等级设置
     */
    public function saveGrade(){
        // dump(I('newNo'));
        $rd = array('status'=>-1);
        $reseller_id = session('reseller_user.pid')==0?session('reseller_user.reseller_id'):session('reseller_user.pid');
        $m = M('public_grade_configs');
        // 保存新的
        $newNo = (int)I('newNo');
        for($i=0;$i<$newNo;$i++){
            $data=array();
            $data['grade_score'] =I('grade_score_n_'.$i);
            $data['grade_name'] =I('grade_name_n_'.$i);
            $data['reseller_id'] = $reseller_id;
            // dump($data);
            // 查重
            $isHave1 = $m->where(array('grade_score'=>$data['grade_score']))
                         ->getField('grade_id');
            $isHave2 = $m->where(array('grade_name'=>$data['grade_name']))
                         ->getField('grade_id');
            if ($isHave1 || $isHave2) {
                continue;
            }else{
                $m->add($data);
            }

        }

        // 更新旧的
        $oldNo = (int)I('oldNo');
        for($i=0;$i<$oldNo;$i++){
            $data=array();
            $map['grade_id'] = I('grade_id_o_'.$i);
            $data['grade_score'] =I('grade_score_o_'.$i);
            $data['grade_name'] =I('grade_name_o_'.$i);
            // 查重
            $isHave1 = $m->where(array('grade_score'=>$data['grade_score'],'grade_id'=>array('neq',$map['grade_id'])))
                         ->getField('grade_id');
            $isHave2 = $m->where(array('grade_name'=>$data['grade_name'],'grade_id'=>array('neq',$map['grade_id'])))
                         ->getField('grade_id');
            if ($isHave1 || $isHave2) {
                continue;
            }else{
                $m->where(array($map))->save($data);
            }
        }



        $rd['status'] = 1;
        return $rd;

    }
}

