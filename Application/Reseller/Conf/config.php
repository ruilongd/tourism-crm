<?php
return array(
	//'配置项'=>'配置值'
	'URL_MODEL' => 2,
	'VIEW_PATH' => './Tpl/',
	'DEFAULT_THEME' => 'Reseller',
    'LOAD_EXT_CONFIG' => 'auth_config',//权限配置
	'TMPL_PARSE_STRING' => array(
        '__PUBLIC__'         => '/Public',
        '__STATIC__'         => '/Public/statics',
        '__ADMIN_ACEADMIN__' => __ROOT__.'/Public/statics/aceadmin',
        '__PUBLIC_CSS__'     => __ROOT__.'/Tpl/Reseller/Public/css',
        '__PUBLIC_JS__'      => __ROOT__.'/Tpl/Reseller/Public/js',
        '__JS__'             => __ROOT__.'/Tpl/Reseller/Public/js',
        '__CSS__'             => __ROOT__.'/Tpl/Reseller/Public/css',
	),
	'SHOW_PAGE_TRACE' => false,
    'AUTH_CONFIG'=>array(
        'AUTH_ON'           => true, // 认证开关
        'AUTH_TYPE'         => 1, // 认证方式，1为实时认证；2为登录认证。
        'AUTH_GROUP'        =>C('DB_PREFIX'). 'reseller_auth_group', // 用户组数据表名
        'AUTH_GROUP_ACCESS' => C('DB_PREFIX').'reseller_auth_group_access', // 用户-用户组关系表
        'AUTH_RULE'         => C('DB_PREFIX').'reseller_auth_rule', // 权限规则表
        'AUTH_USER'         => C('DB_PREFIX').'operator_line_reseller', // 用户信息表
    ),

);
